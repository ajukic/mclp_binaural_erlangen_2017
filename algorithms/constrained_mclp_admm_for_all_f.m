function [G_con,res,kNum,kDen] = constrained_mclp_admm_for_all_f(invQ, xx, G_unc, sig, varargin)
%
% Solve a constrained problem for each f
%
%   splitting variable is z
%
% Note: the convergence is tracked for each frequency individually, and only the bins which have not
% converged are updated.
%

    % ----------------------------------------------------------------------------------------------
    % admm parameters
    % ----------------------------------------------------------------------------------------------
    parser = inputParser;

    parser.addParameter( 'it_max' , 40 );
    parser.addParameter( 'rel_tol', 1e-6 );
    parser.addParameter( 'abs_tol', 1e-6 );
    parser.addParameter( 'rho', 10, @(x) isscalar(x) & x>=0 );
    parser.addParameter( 'gam', 1, @(x) isscalar(x) & x>=1 & x<=((sqrt(5)+1)/2) );
    parser.addParameter( 'show_messages', 0 );

    parser.parse(varargin{:});

    it_max  = parser.Results.it_max;
    rel_tol = parser.Results.rel_tol;
    abs_tol = parser.Results.abs_tol;
    rho     = parser.Results.rho;
    gam     = parser.Results.gam;
    show_messages  = parser.Results.show_messages;

    % ----------------------------------------------------------------------------------------------
    % initialization
    % ----------------------------------------------------------------------------------------------

    % number of frequencies
    [ML_g,F] = size(xx);
    % number of microphones
    M = size(G_unc,2);
    % number of variables for each f
    N_var = ML_g * M; % numel( G_unc(:,:,1) );
    % number of constraints for each f (should be equal to M)
    N_con = size( sig, 1 );

    % initalize the splitting variables
    z  = zeros(N_con,F);
    mu = zeros(N_con,F);
    % other variables
    G_con = zeros(ML_g,M,F);
    estRev_con = zeros(M,F);

    z_res = zeros(M,F);

    % projection function
    myProj = @(var_z,var_sig) min( 1, var_sig ./ ( abs(var_z) + eps ) ) .* var_z;

    % residuals
    res.pri = zeros(F,1);
    res.dua = zeros(F,1);
    res.eps_pri = zeros(F,1);
    res.eps_dua = zeros(F,1);

    % ----------------------------------------------------------------------------------------------
    % iterative algorithm
    % ----------------------------------------------------------------------------------------------

    estRev_unc = filter_for_all_f( G_unc, xx );
    [k_con, kNum, kDen] = gain_vector_for_all_f( invQ, xx, 2, rho*ones(F,1) );

    % list of frequencies to update
    fUp = 1:F;

    % iterations
    for it=1:it_max

        % previous estimate
        z_old = z;

        % estimate the constrained filer G_con
        G_tmp = z(:,fUp) + mu(:,fUp) - estRev_unc(:,fUp);
        G_tmp = bsxfun( @times, reshape( k_con(:,fUp), [ML_g,1,numel(fUp)] ), reshape( conj(G_tmp),[1,M,numel(fUp)] ) );
        G_con(:,:,fUp) = G_unc(:,:,fUp) + G_tmp;

        % predicted reverberant component using the constrained filter
        estRev_con(:,fUp) = filter_for_all_f( G_con(:,:,fUp), xx(:,fUp) );

        % estimate z
        % - clipping of the estimated reverberant component, so that it satisfies the constrains
        z_tmp    = estRev_con(:,fUp) - mu(:,fUp);
        z(:,fUp) = myProj( z_tmp, sig(:,fUp) );

        % residual
        z_res(:,fUp) = z(:,fUp) - estRev_con(:,fUp);

        % update the multipliers
        mu(:,fUp) = mu(:,fUp) + gam * z_res(:,fUp);

        % primal and dual residual
        res.pri(fUp) = sqrt( sum(abs( z_res(:,fUp) ).^2) );
        res.dua(fUp) = rho *  sqrt( sum(abs( z(:,fUp) - z_old(:,fUp) ).^2) );

        % tolerances
        res.eps_pri(fUp) = abs_tol * sqrt(N_con) + ...
                      rel_tol * max( [ sqrt( sum(abs( z(:,fUp) ).^2) ) , sqrt( sum(abs( estRev_con(:,fUp) ).^2) ) ] );
        res.eps_dua(fUp) = abs_tol * sqrt(N_var) + rel_tol * rho * sqrt( sum( abs( mu(:,fUp) ).^2 ) );

        % check convergence
        % - update frequencies where both conditions are not fulfilled
        fUp = find( and( res.pri<res.eps_pri, res.dua<res.eps_dua ) == 0 );

        % stop if all frequencies converged
        if isempty( fUp )
            if show_messages
                fprintf('\nADMM converged for all f at iteration it=%d\n',it)
            end
            % exit the loop
            break
        end

%         if any( any( abs(G_con(:))>1000 ) )
%             warning('I"m inside')
%             keyboard
%         end

    end

end
