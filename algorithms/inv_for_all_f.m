function invQ_n = inv_for_all_f(gam,k_n,xx_n,invQ_prev)
%
% Compute for each f
%
%   invQ = 1/gam * ( invQ_prev - k_n * xx_n' * invQ_prev )
%
%

    [ML_g,F] = size(xx_n);

    tmp = bsxfun( @times, reshape( conj(xx_n), [ML_g,1,F] ), invQ_prev );
    tmp = sum(tmp,1);
    tmp = bsxfun( @times, reshape( k_n, [ML_g,1,F] ), reshape( tmp, [1,ML_g,F] ) );
    
    invQ_n = 1./gam * (invQ_prev - tmp);

end