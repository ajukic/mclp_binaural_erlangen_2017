classdef CON_REC_GWPE < handle
% BLOCK_GWPE online (recursive) implementation of GWPE.
%
%

% ******************************************************************************
% PROPERTIES
% ******************************************************************************

properties

    % number of channels
    M      = 2;
    % number of frequency bins
    F      = 257;
    % filter length
    Lg     = 10;
    % prediction delay
    tau    = 2;
    % weight type
    weightType = 'rev';
    % shape
    pnorm  = 0;
    % regularization
    myeps  = 1e-8;
    % forgetting factor
    gam    = 0.99;
    % sampling frequency
    fs     = [];
    % reverberation time for the Lebart's model [s]
    RT60   = 0.5;
    % time when the late reverb starts [s]
    timeLate = [];
    % windows shift [s]
    winShift = [];
    % constraint type
    constrType = 'Lebart';
    % maximum number of iterations for the ADMM subproblem
    ADMM_itMax = 20;
    % penalty parameter for the ADMM subproblem
    ADMM_rho    = 100;
    % type of smoother
    smType = 'ceps'
    % smoother for the reverberant signal power
    smRev = 0;
    % smoother for the desired signal power
    smDes = 0;

    % history
    buffer       = [];
    G_prev       = [];
    invQ_prev    = [];
    % smoothed reverberant power
    revPowSmooth = [];
    % smoothed desired power
    desPowSmooth = [];
    % late reverb power
    lateRevPow = [];
    % Lebart
    decayConst = [];
    % noise power
    noisePow = [];
    % undesired power (late rev + noise)
    undesPow = [];
    
    % minimum statistics
    minStatConfig = [];
    minStatState  = [];
    
    % cepstral smoothing
    cepConfig   = [];
    cepStateRev = [];
    cepStateDes = [];
    
    % lower limit for SNR
    SNR_LOW_LIM = [];
    
    % oracle
    lateRevMagOracle = [];
    
    % debug
    debug = {};

end

% ******************************************************************************
% METHODS
% ******************************************************************************

methods

    % --------------------------------------------------------------------------
    % constructor
    % --------------------------------------------------------------------------

    function ConRec_GWPE = CON_REC_GWPE(params)

        % set the parameters
        ConRec_GWPE.M           = params.numMics;
        ConRec_GWPE.F           = params.numFreqBins;
        ConRec_GWPE.Lg          = params.filtLen;
        ConRec_GWPE.tau         = params.predDelay;
        ConRec_GWPE.weightType  = params.weightType;
        ConRec_GWPE.pnorm       = params.pnorm;
        ConRec_GWPE.myeps       = params.myeps;
        ConRec_GWPE.gam         = params.gam;
        ConRec_GWPE.fs          = params.fs;
        ConRec_GWPE.RT60        = params.RT60;
        ConRec_GWPE.timeLate    = params.timeLate;
        ConRec_GWPE.winShift    = params.winShift;
        
        ConRec_GWPE.constrType  = params.constrType;
        ConRec_GWPE.ADMM_rho    = params.ADMM_rho;
        ConRec_GWPE.ADMM_itMax  = params.ADMM_itMax;
        ConRec_GWPE.smType      = params.smType;
        ConRec_GWPE.smRev       = params.smRev;
        ConRec_GWPE.smDes       = params.smDes;

        % initialize the history
        ConRec_GWPE.Reset;

    end

    % --------------------------------------------------------------------------
    % reset
    % --------------------------------------------------------------------------

    function Reset(ConRec_GWPE)

        ConRec_GWPE.buffer    = zeros( ConRec_GWPE.M * (ConRec_GWPE.tau + ConRec_GWPE.Lg), ConRec_GWPE.F );
        ConRec_GWPE.G_prev    = zeros( ConRec_GWPE.M * ConRec_GWPE.Lg, ConRec_GWPE.M, ConRec_GWPE.F );
        ConRec_GWPE.invQ_prev = repmat( eye(ConRec_GWPE.M * ConRec_GWPE.Lg), [1,1,ConRec_GWPE.F] );

        % smoothed reverberant power buffer
        ConRec_GWPE.revPowSmooth = zeros( ConRec_GWPE.M, ConRec_GWPE.F, 10 );
        
        % smoothed desired power
        ConRec_GWPE.desPowSmooth = zeros( ConRec_GWPE.M, ConRec_GWPE.F );
        
        % noise power
        ConRec_GWPE.noisePow = zeros( ConRec_GWPE.M, ConRec_GWPE.F );
        
        % undesired power
        ConRec_GWPE.undesPow = zeros( ConRec_GWPE.M, ConRec_GWPE.F );
        
        % minimum statistics
        ConRec_GWPE.minStatConfig.fL        = 2 * (ConRec_GWPE.F-1);
        ConRec_GWPE.minStatConfig.fShift    = round( ConRec_GWPE.winShift * ConRec_GWPE.fs );
        ConRec_GWPE.minStatConfig.fs        = ConRec_GWPE.fs;
        ConRec_GWPE.minStatConfig.bufferLen = 3;
        ConRec_GWPE.minStatState = cell( ConRec_GWPE.M, 1 );
    
        % cepstral smoothing
        ConRec_GWPE.cepConfig   = 1;
        ConRec_GWPE.cepStateRev = cell( ConRec_GWPE.M, 1 );
        ConRec_GWPE.cepStateDes = cell( ConRec_GWPE.M, 1 );
        
        % lower limit on a priori SNR
        ConRec_GWPE.SNR_LOW_LIM = 10.^(-Inf ./ 10);

    end

    % --------------------------------------------------------------------------
    % set weightType
    % --------------------------------------------------------------------------

    function set.weightType(ConRec_GWPE, val)

        switch val
            case {0,'rev'}
                ConRec_GWPE.weightType = 'rev';
            case {1,'des'}
                ConRec_GWPE.weightType = 'des';
            otherwise
                error('Weight type must be either {0 or "rev"} or {1 or "des"}.')
        end

    end
    
    % --------------------------------------------------------------------------
    % set pnorm
    % --------------------------------------------------------------------------

    function set.pnorm(ConRec_GWPE, val)

        if (val>=0) && (val<=2)
            ConRec_GWPE.pnorm = val;
        else
            error('Shape (pnorm) must be in [0,2].')
        end

    end

    % --------------------------------------------------------------------------
    % set myeps
    % --------------------------------------------------------------------------

    function set.myeps(ConRec_GWPE, val)

        if (val>=0)
            ConRec_GWPE.myeps = val;
        else
            error('Regularization (myeps) must be positive.')
        end

    end

    % --------------------------------------------------------------------------
    % set gam
    % --------------------------------------------------------------------------

    function set.gam(ConRec_GWPE, val)

        if (val>0) && (val<=1)
            ConRec_GWPE.gam = val;
        else
            error('Forgetting factor must be in (0,1]')
        end

    end

    % --------------------------------------------------------------------------
    % set tau (delay)
    % --------------------------------------------------------------------------
    % set tau and reset the algorithm
    %

    function set.tau(ConRec_GWPE, val)

        if (val>0)
            ConRec_GWPE.tau = val;
        else
            error('tau (delay) must be positive integer')
        end

        % reset the algorithm
        ConRec_GWPE.Reset;

    end

    % --------------------------------------------------------------------------
    % set Lg (filter length)
    % --------------------------------------------------------------------------
    % set Lg and reset the algorithm
    %

    function set.Lg(ConRec_GWPE, val)

        if (val>0)
            ConRec_GWPE.Lg = val;
        else
            error('Lg (filter length) must be positive integer')
        end

        % reset the algorithm
        ConRec_GWPE.Reset;

    end

    % --------------------------------------------------------------------------
    % set RT60
    % --------------------------------------------------------------------------

    function set.RT60(ConRec_GWPE, val)

        if (val>0)
            ConRec_GWPE.RT60 = val;
        elseif(isempty(val))
            warning('Reverberation time (RT60) for the Lebart"s model  *not given*.')
        else
            error('Reverberation time (RT60) for the Lebart"s model  must be positive.')
        end

    end

    % --------------------------------------------------------------------------
    % set ADMM_rho
    % --------------------------------------------------------------------------

    function set.ADMM_rho(ConRec_GWPE, val)

        if (val>0)
            ConRec_GWPE.ADMM_rho = val;
        else
            error('ADMM penalty parameter (ADMM_rho) must be positive.')
        end

    end

    % --------------------------------------------------------------------------
    % set ADMM_itMax
    % --------------------------------------------------------------------------

    function set.ADMM_itMax(ConRec_GWPE, val)

        if (val>=0)
            ConRec_GWPE.ADMM_itMax = val;
        else
            error('ADMM maximum number of iterations (ADMM_itMax) must be positive or 0 (to turn off ADMM).')
        end

    end

    % --------------------------------------------------------------------------
    % set smRev
    % --------------------------------------------------------------------------

    function set.smRev(ConRec_GWPE, val)

        if (val>=0) && (val<=1)
            ConRec_GWPE.smRev = val;
        else
            error('Rev. power smoother (smRev) must be in [0,1].')
        end

    end
    
    % --------------------------------------------------------------------------
    % set smDes
    % --------------------------------------------------------------------------

    function set.smDes(ConRec_GWPE, val)

        if (val>=0) && (val<=1)
            ConRec_GWPE.smDes = val;
        else
            error('Des. power smoother (smDes) must be in [0,1].')
        end

    end

    % --------------------------------------------------------------------------
    % set smDes
    % --------------------------------------------------------------------------

    function set.lateRevMagOracle(ConRec_GWPE, val)

        ConRec_GWPE.lateRevMagOracle = val;

    end
    
    % --------------------------------------------------------------------------
    % update buffer
    % --------------------------------------------------------------------------

    function updateBuffer(ConRec_GWPE, in)
    % update the buffer with new time step
    %
    % buffer = [ squeeze( X(:,n,:) ) ; buffer(1:end-M,:) ];
    %

        % shift the old
        ConRec_GWPE.buffer(ConRec_GWPE.M+1 : end, :) = ConRec_GWPE.buffer(1 : end-ConRec_GWPE.M, :);

        % in with the new
        ConRec_GWPE.buffer(1 : ConRec_GWPE.M, :) = in;

    end

    % --------------------------------------------------------------------------
    % delayed buffer
    % --------------------------------------------------------------------------

    function out = delayedBuffer(ConRec_GWPE)
    % return the buffer delayed for tau time steps (tau*M)
    %

        out = ConRec_GWPE.buffer( ConRec_GWPE.M * ConRec_GWPE.tau + 1 : end, : );

    end

    % --------------------------------------------------------------------------
    % delayed signal
    % --------------------------------------------------------------------------

    function out = delayedSignal(ConRec_GWPE, nDelay)
    % return the M-channel signal value delayed for nDelay time steps
    % - this corresponds to skipping (nDelay*M) samples, since the buffer is
    %   written in block of M samples (since there are M channels)

        out = ConRec_GWPE.buffer( ConRec_GWPE.M * nDelay + 1 : ConRec_GWPE.M * (nDelay + 1), : );

    end
    
    % --------------------------------------------------------------------------
    % estimate late reverb using a decay model
    % --------------------------------------------------------------------------

    function [] = updatePowerEstimators(ConRec_GWPE)
	% update the recursive power estimators (for all channels) and
    % return the estimate of the late reverberant power based on Lebart's model
    %

        % parameters for the late reverberation estimator
        decaySigma = 6.908 ./ ConRec_GWPE.RT60;
        nLate      = round( ConRec_GWPE.timeLate / ConRec_GWPE.winShift );
        ConRec_GWPE.decayConst = exp( -2 * decaySigma * ConRec_GWPE.timeLate );
    
        switch ConRec_GWPE.smType
            
            case 'spec'
            % recursive smoothing of spectral power
        
                % estimation of the reverberant power
                % --------------------------------------------------------------
                revPowSm = ConRec_GWPE.smRev * ConRec_GWPE.revPowSmooth(:,:,1) + ...
                            (1-ConRec_GWPE.smRev) * abs( ConRec_GWPE.delayedSignal(0) ).^2;

                    
                % shift the buffer
                ConRec_GWPE.revPowSmooth(:,:,2:end) = ConRec_GWPE.revPowSmooth(:,:,1:end-1);
                ConRec_GWPE.revPowSmooth(:,:,1)     = revPowSm;
                
%                 % estimation the late reverberant power (Lebart's model)
%                 % --------------------------------------------------------------
% 
%                 ConRec_GWPE.lateRevPow = ConRec_GWPE.decayConst * squeeze( ConRec_GWPE.revPowSmooth(:,:,nLate) );
% 
%                 % estimation of the desired power
%                 % --------------------------------------------------------------
% 
%                 desPow = max(ConRec_GWPE.revPowSmooth(:,:,1) - ConRec_GWPE.lateRevPow, ConRec_GWPE.myeps);
% 
%                 ConRec_GWPE.desPowSmooth = ...
%                     ConRec_GWPE.smDes * ConRec_GWPE.desPowSmooth + (1-ConRec_GWPE.smDes) * desPow;
                
            case 'ceps'
            % recursive cepstral smoothing
            
                inputSig = ConRec_GWPE.delayedSignal(0);
            
                % noise tracking
                % --------------------------------------------------------------
                
                for m=1:ConRec_GWPE.M
                    if isempty( ConRec_GWPE.minStatState{m} )
                        % initialize minimum statistics estimator
                        [ConRec_GWPE.noisePow(m,:), ConRec_GWPE.minStatState{m}] = ...
                            minimumStatistics2001( abs(inputSig(m,:)).', 10, [], ConRec_GWPE.minStatConfig, true);
                    else
                        % process minimum statistics estimator
                        [ConRec_GWPE.noisePow(m,:), ConRec_GWPE.minStatState{m}] = ...
                            minimumStatistics2001( abs( inputSig(m,:) ).', 10, ConRec_GWPE.minStatState{m}, ConRec_GWPE.minStatConfig, false);
                    end
                end
                
                % estimation of the reverberant power
                % --------------------------------------------------------------
                
                for m=1:ConRec_GWPE.M
                    
                    if isempty( ConRec_GWPE.cepStateRev{m} )
                        % initialize minimum statistics estimator
                        [snrPrio(m,:), ConRec_GWPE.cepStateRev{m}] = ...
                            cepSNRREVERB( abs(inputSig(m,:)).', ConRec_GWPE.noisePow(m,:).', ConRec_GWPE.fs, true, [], ConRec_GWPE.cepConfig);
                    else
                        % process minimum statistics estimator
                        [snrPrio(m,:), ConRec_GWPE.cepStateRev{m}] = ...
                            cepSNRREVERB( abs( inputSig(m,:) ).', ConRec_GWPE.noisePow(m,:).', ConRec_GWPE.fs, false, ConRec_GWPE.cepStateRev{m}, ConRec_GWPE.cepConfig);
                    end
                    
                end
                
                snrPrio = max(snrPrio, ConRec_GWPE.SNR_LOW_LIM);
                
                % shift the buffer
                ConRec_GWPE.revPowSmooth(:,:,2:end) = ConRec_GWPE.revPowSmooth(:,:,1:end-1);
                ConRec_GWPE.revPowSmooth(:,:,1)     = snrPrio .* ConRec_GWPE.noisePow;
                
%                 % estimation the late reverberant power (Lebart's model)
%                 % --------------------------------------------------------------
%                 
%                 ConRec_GWPE.lateRevPow = ConRec_GWPE.decayConst * squeeze( ConRec_GWPE.revPowSmooth(:,:,nLate) );
%                 
%                 % estimation of the desired power
%                 % --------------------------------------------------------------
%                 
%                 ConRec_GWPE.undesPow = ConRec_GWPE.noisePow + ConRec_GWPE.lateRevPow;
%                 
%                 for m=1:ConRec_GWPE.M
%                     
%                     if isempty( ConRec_GWPE.cepStateDes{m} )
%                         % initialize minimum statistics estimator
%                         [desSnrPrio(m,:), ConRec_GWPE.cepStateDes{m}] = ...
%                             cepSNRREVERB( abs(inputSig(m,:)).', ConRec_GWPE.undesPow(m,:).', ConRec_GWPE.fs, true, [], ConRec_GWPE.cepConfig);
%                     else
%                         % process minimum statistics estimator
%                         [desSnrPrio(m,:), ConRec_GWPE.cepStateDes{m}] = ...
%                             cepSNRREVERB( abs( inputSig(m,:) ).', ConRec_GWPE.undesPow(m,:).', ConRec_GWPE.fs, false, ConRec_GWPE.cepStateDes{m}, ConRec_GWPE.cepConfig);
%                     end
%                     
%                 end
%                 
%                 desSnrPrio = max(desSnrPrio, ConRec_GWPE.SNR_LOW_LIM);
%             
%                 ConRec_GWPE.desPowSmooth = desSnrPrio .* ConRec_GWPE.undesPow;
                
        end

    end
        
    % --------------------------------------------------------------------------
    % compute the weights
    % --------------------------------------------------------------------------
    
    function weights = computeWeights(ConRec_GWPE)
    % this is still using the identity spatial correlation
    %
    %
        
        switch ConRec_GWPE.weightType
            case 'rev'
                selPow = ConRec_GWPE.revPowSmooth(:,:,1);
            case 'des'
                selPow = ConRec_GWPE.desPowSmooth;
        end
        
        weights = ( mean(selPow,1) + ConRec_GWPE.myeps ).^(ConRec_GWPE.pnorm/2-1);
        weights = weights(:);
        
    end

    % --------------------------------------------------------------------------
    % bound for the late reverberation
    % --------------------------------------------------------------------------
    
    function out = lateRevMagBound(ConRec_GWPE, n)
    %
    %
        switch ConRec_GWPE.constrType
            case 'Lebart'
                out = sqrt(ConRec_GWPE.lateRevPow);
            case 'oracle'
                out = squeeze( ConRec_GWPE.lateRevMagOracle(:,n,:) ).';
        end
        
    end
    
    % --------------------------------------------------------------------------
    % processing
    % --------------------------------------------------------------------------

    function [outLin, outSS] = Process(ConRec_GWPE, in)
    % process all time blocks in the input
    %

        % number of blocks to process
        Nb = size(in,2);

        % input coefficients (M x Nb x F)
        X = permute(in, [3,2,1]);

        % output coefficients
        Dlin = zeros( size(X) );
        Dss  = zeros( size(X) );

        % processing
        for n=1:Nb

            % ------------------------------------------------------------------
            % update the buffer
            % ------------------------------------------------------------------

            ConRec_GWPE.updateBuffer( X(:,n,:) );

            % add two smoothers: for the reverberant power and another one for
            % the 'clean' power
            
            % ------------------------------------------------------------------
            % estimate late reverberant power
            % ------------------------------------------------------------------
            % use Lebart's model with the given RT60 to get an estimate of the
            % late reverberant power
            %
            % delay for the late reverberation is defined using the prediction
            % delay tau
            %

            ConRec_GWPE.updatePowerEstimators;

            % ------------------------------------------------------------------
            % compute the weights
            % ------------------------------------------------------------------

            w_n = ConRec_GWPE.computeWeights;

            % ------------------------------------------------------------------
            % update prediction filters
            % ------------------------------------------------------------------

            % compute the gain vector
            k_n = gain_vector_for_all_f( ConRec_GWPE.invQ_prev, ConRec_GWPE.delayedBuffer, ConRec_GWPE.gam, w_n );

            % update the filters
            G_cor_n = filter_correction_for_all_f( k_n, squeeze(X(:,n,:)), ConRec_GWPE.G_prev, ConRec_GWPE.delayedBuffer );

            % apply the correction
            G_n     = ConRec_GWPE.G_prev + G_cor_n;

            % compute the current inverse
            invQ_n = inv_for_all_f( ConRec_GWPE.gam, k_n, ConRec_GWPE.delayedBuffer, ConRec_GWPE.invQ_prev );

            % get the constrained filters
            % - but first check if ADMM is to be used at all
            if ConRec_GWPE.ADMM_itMax<=0
                % optimizer is not used - forward the unconstrained filter
                G_con_n = G_n;
            else
                
                % estRevMagBound = sqrt(ConRec_GWPE.lateRevPow);
                estRevMagBound = ConRec_GWPE.lateRevMagBound(n);
                
                % use ADMM optimizer
                G_con_n = constrained_mclp_admm_for_all_f( invQ_n, ...
                                                           ConRec_GWPE.delayedBuffer, ...
                                                           G_n, ...
                                                           estRevMagBound, ...
                                                           'rho', ConRec_GWPE.ADMM_rho, ...
                                                           'it_max', ConRec_GWPE.ADMM_itMax );
            end
            
            % ------------------------------------------------------------------
            % dereverberation
            % ------------------------------------------------------------------

            % estimate the reverberant component
            estRev_n = filter_for_all_f( G_con_n, ConRec_GWPE.delayedBuffer );

            % estimate the desired speech -> linear
            Dlin(:,n,:) = squeeze( X(:,n,:) ) - estRev_n;

            % estimate the desired speech -> spectral subtraction
            Dss(:,n,:)  = sqrt( max(abs( squeeze(X(:,n,:)) ).^2 - abs(estRev_n).^2, 0) ) ...
                                               .* exp( 1j * angle( squeeze(X(:,n,:)) ) );
                                           
                    
%             if any( any( abs(Dlin(:,n,:))>1000 ) )
%                 warning('I"m here')
%                 figure(1), imagesc( log10(abs( squeeze( Dlin(1,:,:) ) )).' ), axis xy, colorbar
%                 keyboard
%             end
            
            % ------------------------------------------------------------------
            % save history
            % ------------------------------------------------------------------
            ConRec_GWPE.G_prev    = G_n;
            ConRec_GWPE.invQ_prev = invQ_n;
            
            % save for debugging
            % ------------------------------------------------------------------
            ConRec_GWPE.debug{1}(:,n,:) = ConRec_GWPE.revPowSmooth(:,:,1).';
            ConRec_GWPE.debug{2}(:,n,:) = ConRec_GWPE.desPowSmooth.';
            ConRec_GWPE.debug{3}(:,n,:) = ConRec_GWPE.lateRevPow.';
            ConRec_GWPE.debug{4}(:,n,:) = estRev_n.';

        end

        % output is in format F x Nb x M
        outLin = permute(Dlin, [3,2,1]);
        outSS  = permute(Dss, [3,2,1]);

    end

end % end methods


end % end classdef
