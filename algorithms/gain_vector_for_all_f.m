function [k_n, kNum, kDen] = gain_vector_for_all_f(invQ_prev,xx_n,gam,w_n)
% Computes the gain vector for all frequencies simultaneously.
%
% This function computes the following for each f
%
%   k_n(:,f) = invQ_prev(:,:,f) * xx_n(:,f) / ( gam(f)/w_n(f) + xx_n(:,f)' * invQ_prev(:,:,f) * xx_n(:,f) )
%

    [MLg,F] = size(xx_n);

    % numerator
    kNum = bsxfun( @times, invQ_prev, reshape(xx_n,[1,MLg,F]) );
    kNum = squeeze( sum(kNum,2) );

    % denominator
    kDen = bsxfun( @times, conj(xx_n), kNum );
    kDen = sum(kDen,1);

    % divide
    k_n = bsxfun( @rdivide, kNum, kDen + gam./w_n.' );
    % k_n = bsxfun( @times, kNum, 1./( kDen + gam./w_n.' ) );

end