function [Dout] = alg_nb_irls(xRev,varargin)
%
% This is a generalization of the WPE algorithm
%
% This is an MCLP-based dereverberation approach with a narrow-band (NB) constraint.
%
% We estimate the STFT coefficients of the desired speech signal, while
% the cost function enforces sparsity in the STFT domain, i.e., this is synthesis
% type of problem.
%
%
% Note that the (I)STFT transformation is performed using the LTFAT toolbox
% that is available online [3].
%
% INPUT
%   xRev           : signals on microphones, each column corresponds to
%                     one microphone
%   varargin        : structure containing the parameters
%       .refMic     : reference microphone
%       .tau        : prediction delay in frames (equal for all frequencies) or a set of delays
%                     (individual value for each of the bins).
%                     [2]
%       .Lg         : order of regression filters (equal for all frequencies)
%                     or a set of orders of regression filters
%                     (individual value for each of the bins)
%                     [default=20, equal for all freq. bins]
%       .nFFT       : frame length (in samples)
%                     [1024]
%       .nShift     : frame shift (in samples)
%                     [256]
%       .winType    : window type for the STFT
%                     ['hamming']
%       .myeps      : lower bound for estimation of variance,
%                     small positive constant to avoid zero division
%                     [1e-8]
%       .itMax      : maximal number of iterations
%                     [10]
%       .itTol      : tolerance on relative change of the estimate
%                     [1e-4]
%       .p          : parameter for updating weights/variances, corresponds
%                     to ell_p norm that is used as the objective function
%                     for the desired speech signal
%
% OUTPUT
%   d               : estimated desired speech signal, each column
%                     corresponds to one microphone
%   D               : STFT of the estimated desired speech signal
%
% REFERENCES
% [1] Yoshioka et al., 2012
%     Generalization of multi-channel linear prediction methods for blind
%     MIMO impulse response shortening
%     IEEE Tr. Audio Speech Lang. Process 20 (10), pp. 2707-2720
% [2] Juki?? et al., 2015
%     Group sparsity for MIMO blind speech dereverberation
%     Internal report, University of Oldenburg
% [3] The Large Time-Frequency Analysis Toolbox (LTFAT)
%     http://ltfat.sourceforge.net/
%
%
% Copyright (c) 2011, Ante Jukic
% Author: Ante Jukic
% University of Oldenburg
% Contact: ante.jukic@uni-oldenburg.de
% All rights reserved.
%
% This software is not in the public domain.
% Redistribution in source and binary forms, with or without
% modification, is not permitted.
%
% Neither the name of the University of Oldenburg nor the names of the
% contributors may be used to endorse or promote products derived from this
% software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
%
% Version v1.0 (Feb 2015)
%

%%

% number of microphones
M = size(xRev,2);


%% PARAMETERS

parser = inputParser;

parser.addParamValue('tau'      , 2         );
parser.addParamValue('Lg'       , 10        );
parser.addParamValue('nFFT'     , 1024      );
parser.addParamValue('nShift'   , 256       );
parser.addParamValue('winType'  , 'hann' );
parser.addParamValue('myeps'    , 1e-8      );
parser.addParamValue('itMax'    , 10        );
parser.addParamValue('relTol'   , 1e-6      );
parser.addParamValue('normInput', 0         );
% parameter for estimation of variances - related to the p-norm of the desired signal in the IRLS procedure
parser.addParamValue( 'p'       , 0         , @(x) isscalar(x) & x >= 0 & x<=2 );

parser.parse(varargin{:});

tau       = parser.Results.tau;
Lg        = parser.Results.Lg;
nFFT      = parser.Results.nFFT;
nShift    = parser.Results.nShift;
winType   = parser.Results.winType;
myeps     = parser.Results.myeps;
itMax     = parser.Results.itMax;
relTol    = parser.Results.relTol;
normInput = parser.Results.normInput;
% ell_p norm to minimize
p         = parser.Results.p;


%% normalize the time-domain signal

if normInput==1
    scaleTimeDomain = max( abs(xRev(:)) );
    
    xRev = xRev / scaleTimeDomain;
end


%% STFT transform

% use a tight window for analysis and synthesis
w = {'tight',winType,nFFT};

% STFT transform
[X,Lx] = dgtreal(xRev,w,nShift,nFFT);

K = size(X,1);  % number of freq. bins
N = size(X,2);  % number of time frames


%% DEREVERBERATION

% prediction delay
if numel(tau)==1
    tau = tau * ones(K,1);
elseif numel(tau)~=K
    error('Parameter .tau should consist of 1 or K elements.')
end

% length of the prediction filter for each channel
if numel(Lg)==1
    Lg = Lg * ones(K,1);
elseif numel(Lg)~=K
    error('Parameter .Lg should consist of 1 or K elements.')
end

% estimated desired speech signal
D = zeros(K,N,M);

% -------------------------------------------------------------------------
% Independent processing in each frequency bin
% -------------------------------------------------------------------------
%
% Each IRLS iteration is composed of the following steps
% (1) calculation of the weights, based on the current estimate (using the selected p)
% (2) [optional] update of the correlation matrix
% (3) estimation of the prediction filters by solving a weighted L2 norm minimization problem
%

for k=1:K
    
    % initialize aux. convolution matrices
    Xref = squeeze( X(k,1:N,1:M) );
    
    XX   = zeros(N,M*Lg(k));
    for m=1:M
        tmp = convmtx( [ zeros( tau(k), 1 ) ; squeeze(X(k,:,m)).' ] , Lg(k) );
        tmp = tmp(1:N,:);
        XX( 1:N , (m-1)*Lg(k)+1 : m*Lg(k) ) = tmp;
    end
    
    % initial estimate of the (hopefully) dereverberated signal
    Dk = Xref;
    
    % ---------------------------------------------------------------------
    % IRLS procedure
    % ---------------------------------------------------------------------
    
    for ii=1:itMax
        
        % -----------------------------------------------------------------
        % update the weights
        % -----------------------------------------------------------------
        
        wk = ( sum(abs(Dk).^2,2)/M + myeps ).^(p/2-1);
        
        % -----------------------------------------------------------------
        % update the prediction filters (1 channel output)
        % -----------------------------------------------------------------

        % estimate regression filters
        Qk = XX' * diag( wk ) * XX;
        Bk = XX' * diag( wk ) * Xref;
        
        Gk = Qk \ Bk;
        % gk = inverse(Qk) * bk;

        % -----------------------------------------------------------------
        % estimate the desired speech
        % -----------------------------------------------------------------
        
        Dk = Xref - XX * Gk;        

    end % end IRLS procedure
       
    % save the result
    D(k,:,:) = Dk;

end

%% inverse STFT transformation

Dout = idgtreal(D,w,nShift,nFFT,Lx);

% rescale
% - if normalization was used for the input

if normInput==1
    Dout = Dout * scaleTimeDomain;
end

end
