function [W, H, cost] = beta_nmf_H(V,W,H,varargin)
%
% "Heuristic" multiplicative algorithm for NMF with the beta-divergence
%
% [W, H, cost] = beta_nmf_H(V, beta, n_iter, W_ini, H_ini)
%
% Input:
%   - V: positive matrix data (F x N)
%   - beta : beta-divergence parameter
%   - n_iter: number of iterations
%   - W_ini: basis (F x K)
%   - H_ini: gains (K x N)
%
% Outputs :
%   - W and H such that
%
%               V \approx W * H
%
%   - cost : beta-divergence though iterations

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2011 Cedric Fevotte & Jerome Idier
% This software is distributed under the terms of the GNU Public License
% version 3 (http://www.gnu.org/licenses/gpl.txt)
%
% Please cite the following reference if you use this code
% C. Fevotte & J. Idier, "Algorithms for nonnegative matrix factorization
% with the beta-divergence ", Neural Compuation, 2011.
%
% Please report any bug at fevotte -at- telecom-paristech -dot- fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% parameters

parser = inputParser;

parser.addParamValue('beta'        , 0     );  % Itakura-Saito
parser.addParamValue('update_W'    , 1     );
parser.addParamValue('update_H'    , 1     );
parser.addParamValue('MaxIt'       , 100   );
parser.addParamValue('FunTol'      , 1e-6  );
parser.addParamValue('showMessages', 0     );

parser.parse(varargin{:});

beta     = parser.Results.beta;
update_W = parser.Results.update_W;
update_H = parser.Results.update_H;
MaxIt    = parser.Results.MaxIt;
FunTol   = parser.Results.FunTol;
showMessages  = parser.Results.showMessages;


iterDisp = 100;

%% original function

switch_W = update_W;
switch_H = update_H;
SCALE    = and(switch_W,switch_H) & 1;

[F,N] = size(V);

V_ap = W*H;

cost    = zeros(1,MaxIt);
cost(1) = betadiv(V,V_ap,beta);

disp('Beta NMF started')

switch beta

    case 2 % beta = 2

        for iter = 2:MaxIt

            if switch_W
                W = W .* (V*H')./(W*(H*H'));
            end

            if switch_H
                H = H .* (W'* V)./((W'*W)*H);
            end

            if SCALE
                scale = sum(W,1);
                W = W .* repmat(scale.^-1,F,1);
                H = H .* repmat(scale',1,N);
            end

            cost(iter) = betadiv(V,W*H,beta);
            
            % check convergence
            if iter>1 && abs( ( cost(iter)-cost(iter-1) ) / cost(iter-1) )<FunTol
                cost  = cost(1:iter);
                if showMessages
                    fprintf('\t converged iter=%d, cost=%g\n',iter,cost(iter))
                end
                break;
            end

            if rem(iter,iterDisp)==0
                if showMessages
                    fprintf('\t iter=%d, cost=%g\n',iter,cost(iter))
                end
            end

        end

    case 1 % beta = 1
        
        if switch_W == 0; scale = sum(W,1); end

        for iter = 2:MaxIt

            if switch_W
                W = W .* ((V./V_ap)*H.')./repmat(sum(H,2)',F,1);
                scale = sum(W,1);
                V_ap = W*H;
            end

            if switch_H
                H = H .* (W.'*(V./V_ap))./repmat(scale',1,N);
                V_ap = W*H;
            end

            if SCALE
                scale = sum(W,1);
                W = W .* repmat(scale.^-1,F,1);
                H = H .* repmat(scale',1,N);
            end

            cost(iter) = betadiv(V,V_ap,beta);
            
            % check convergence
            if iter>1 && abs( ( cost(iter)-cost(iter-1) ) / cost(iter-1) )<FunTol
                cost  = cost(1:iter);
                if showMessages
                    fprintf('\t converged iter=%d, cost=%g\n',iter,cost(iter))
                end
                break;
            end

            if rem(iter,iterDisp)==0
                if showMessages
                    fprintf('\t iter=%d, cost=%g\n',iter,cost(iter))
                end
            end

        end

    otherwise

        for iter = 2:MaxIt

            if switch_W
                W = W.*((V.*V_ap.^(beta-2))*H')./(V_ap.^(beta-1)*H');
                V_ap = W*H;
            end

            if switch_H
                H = H .* (W'*(V.*V_ap.^(beta-2)))./(W'*V_ap.^(beta-1));
                V_ap = W*H;
            end

            if SCALE
                scale = sum(W,1);
                W = W .* repmat(scale.^-1,F,1);
                H = H .* repmat(scale',1,N);
            end

            cost(iter) = betadiv(V,V_ap,beta);
            
            % check convergence
            if iter>1 && abs( ( cost(iter)-cost(iter-1) ) / cost(iter-1) )<FunTol
                cost  = cost(1:iter);
                if showMessages
                    fprintf('\t converged iter=%d, cost=%g\n',iter,cost(iter))
                end
                break;
            end

            if rem(iter,iterDisp)==0
                if showMessages
                    fprintf('\t iter=%d, cost=%g\n',iter,cost(iter))
                end
            end

        end
end
