function out = filter_for_all_f(G_n,xx_n)
% Filtering operations for all f simultaneously.
%
% This function computes the following
%
%   out(:,f) = ( G(:,:,f) )^H * xx_n(:,f);
%
    [MLg,F] = size(xx_n);

    % fitering
    tmp = bsxfun( @times, conj(G_n), reshape(xx_n,[MLg,1,F]) );
    out = squeeze( sum(tmp,1) );

    % make sure the output is a column vector if the input signal has only
    % a single column (e.g., a single frequency)
    if size(xx_n,2)==1
        out = out(:);
    end

end
