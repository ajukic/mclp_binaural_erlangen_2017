function G_cor_n = filter_correction_for_all_f(k_n,X_n,G_prev,xx_n)
% Computes the filter correction for all frequencies simultaneously.
%
% This function computes the following for each f
%
%   G_cor_n(:,:,f) = k_n(:,f) * ( X_n(:,f) - G_prev(:,:,f)' * xx_n(:,f) )'
%
    M = size(G_prev,2);
    [MLg,F] = size(xx_n);

    res = X_n - filter_for_all_f(G_prev,xx_n);
    G_cor_n = bsxfun( @times, reshape(k_n,[MLg,1,F]), reshape( conj(res),[1,M,F] ) );

end