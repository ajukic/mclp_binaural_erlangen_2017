classdef BATCH_GWPE < handle
% BLOCK_GWPE online (recursive) implementation of GWPE.
%
%

% ******************************************************************************
% PROPERTIES
% ******************************************************************************

properties

    % number of channels
    M      = 2;
    % number of frequency bins
    F      = 257;
    % filter length
    Lg     = 10;
    % prediction delay
    tau    = 2;
    % weight type
    weightType = 'iter';
    % shape
    pnorm  = 0;
    % maximum number of iterations for IRLS
    itMax = 40;
    % regularization
    myeps  = 1e-8;
    % reverberation time for the Lebart's model [s]
    RT60   = 0.5;
    % time when the late reverb starts [s]
    timeLate = [];
    % windows shift [s]
    winShift = [];
    % smoother for the reverberant signal power
    smRev = 0;
    % smoother for the desired signal power
    smDes = 0;

    % smoothed reverberant power
    revPowSmooth = [];
    % smoothed desired power
    desPowSmooth = [];
    % smoothed delayed reverberant power
    revDelayedPowSmooth = [];
    % late reverb power
    decayConst = [];
    lateRevPow = [];
    
    % debug
    debug = {};

end

% ******************************************************************************
% METHODS
% ******************************************************************************

methods

    % --------------------------------------------------------------------------
    % constructor
    % --------------------------------------------------------------------------

    function Batch_GWPE = BATCH_GWPE(params)

        % set the parameters
        Batch_GWPE.M           = params.numMics;
        Batch_GWPE.F           = params.numFreqBins;
        Batch_GWPE.Lg          = params.filtLen;
        Batch_GWPE.tau         = params.predDelay;
        Batch_GWPE.weightType  = params.weightType;
        Batch_GWPE.itMax       = params.itMax;
        Batch_GWPE.pnorm       = params.pnorm;
        Batch_GWPE.myeps       = params.myeps;
        Batch_GWPE.RT60        = params.RT60;
        Batch_GWPE.timeLate    = params.timeLate;
        Batch_GWPE.winShift    = params.winShift;
        
        Batch_GWPE.smRev       = params.smRev;
        Batch_GWPE.smDes       = params.smDes;

    end

    % --------------------------------------------------------------------------
    % set weightType
    % --------------------------------------------------------------------------

    function set.weightType(Batch_GWPE, val)

        switch val
            case {0,'rev'}
                Batch_GWPE.weightType = 'rev';
            case {1,'des'}
                Batch_GWPE.weightType = 'des';
            case {2,'iter'}
                Batch_GWPE.weightType = 'iter';
            otherwise
                error('Weight type must be either {0 or "rev"}, {1 or "des"}, {2 or "iter"}.')
        end

    end
    
    % --------------------------------------------------------------------------
    % set pnorm
    % --------------------------------------------------------------------------

    function set.pnorm(Batch_GWPE, val)

        if (val>=0) && (val<=2)
            Batch_GWPE.pnorm = val;
        else
            error('Shape (pnorm) must be in [0,2].')
        end

    end

    % --------------------------------------------------------------------------
    % set myeps
    % --------------------------------------------------------------------------

    function set.myeps(Batch_GWPE, val)

        if (val>=0)
            Batch_GWPE.myeps = val;
        else
            error('Regularization (myeps) must be positive.')
        end

    end

    % --------------------------------------------------------------------------
    % set tau (delay)
    % --------------------------------------------------------------------------
    % set tau and reset the algorithm
    %

    function set.tau(Batch_GWPE, val)

        if (val>0)
            Batch_GWPE.tau = val;
        else
            error('tau (delay) must be positive integer')
        end

    end

    % --------------------------------------------------------------------------
    % set Lg (filter length)
    % --------------------------------------------------------------------------
    % set Lg and reset the algorithm
    %

    function set.Lg(Batch_GWPE, val)

        if (val>0)
            Batch_GWPE.Lg = val;
        else
            error('Lg (filter length) must be positive integer')
        end

    end

    % --------------------------------------------------------------------------
    % set RT60
    % --------------------------------------------------------------------------

    function set.RT60(Batch_GWPE, val)

        if (val>0)
            Batch_GWPE.RT60 = val;
        elseif(isempty(val))
            warning('Reverberation time (RT60) for the Lebart"s model  *not given*.')
        else
            error('Reverberation time (RT60) for the Lebart"s model  must be positive.')
        end

    end

    % --------------------------------------------------------------------------
    % set itMax
    % --------------------------------------------------------------------------

    function set.itMax(Batch_GWPE, val)

        if (val>0)
            Batch_GWPE.itMax = val;
        else
            error('IRLS maximum number of iterations (itMax) must be positive.')
        end

    end

    % --------------------------------------------------------------------------
    % set smRev
    % --------------------------------------------------------------------------

    function set.smRev(Batch_GWPE, val)

        if (val>=0) && (val<=1)
            Batch_GWPE.smRev = val;
        else
            error('Rev. power smoother (smRev) must be in [0,1].')
        end

    end
    
    % --------------------------------------------------------------------------
    % set smDes
    % --------------------------------------------------------------------------

    function set.smDes(Batch_GWPE, val)

        if (val>=0) && (val<=1)
            Batch_GWPE.smDes = val;
        else
            error('Des. power smoother (smDes) must be in [0,1].')
        end

    end
    
    % --------------------------------------------------------------------------
    % estimate late reverb using a decay model
    % --------------------------------------------------------------------------

    function [] = updatePowerEstimators(Batch_GWPE, X_f)
	% update the recursive power estimators (for all channels) and
    % return the estimate of the late reverberant power based on Lebart's model
    %

        % smoothing of the reverberant power
        % ----------------------------------------------------------------------
        Batch_GWPE.revPowSmooth = filter(1-Batch_GWPE.smRev, [1, -Batch_GWPE.smRev] , abs(X_f).^2);
                
        % estimate the late reverberant power
        % ----------------------------------------------------------------------
        
        % parameters for the late reverberation estimator
        decaySigma = 6.908 ./ Batch_GWPE.RT60;
        nLate      = round( Batch_GWPE.timeLate / Batch_GWPE.winShift );
        Batch_GWPE.decayConst = exp( -2 * decaySigma * Batch_GWPE.timeLate );

        % smoothing of the delayed reverberant power
        Batch_GWPE.revDelayedPowSmooth = filter(1-Batch_GWPE.smRev, [1, -Batch_GWPE.smRev] , [ zeros(nLate,Batch_GWPE.M) ; abs(X_f(1:end-nLate,:)).^2 ]);
            
        % Lebart's model
        Batch_GWPE.lateRevPow = Batch_GWPE.decayConst * Batch_GWPE.revDelayedPowSmooth;
            
        % smoothing of the desired power
        % ----------------------------------------------------------------------
        
        desPow = max(Batch_GWPE.revPowSmooth - Batch_GWPE.lateRevPow, 0);
        
        Batch_GWPE.desPowSmooth = filter(1-Batch_GWPE.smDes, [1, -Batch_GWPE.smDes] , desPow);
        
    end
        
    % --------------------------------------------------------------------------
    % compute the weights
    % --------------------------------------------------------------------------
    
    function weights = computeWeights(Batch_GWPE, D_f)
    % this is still using the identity spatial correlation
    %
    %
        
        switch Batch_GWPE.weightType
            case 'rev'
                selPow = Batch_GWPE.revPowSmooth;
            case 'des'
                selPow = Batch_GWPE.desPowSmooth;
            case 'iter'
                selPow = abs(D_f).^2;
        end
        
        weights = ( mean(selPow,2) + Batch_GWPE.myeps ).^(Batch_GWPE.pnorm/2-1);
        weights = weights(:);
        
    end

    % --------------------------------------------------------------------------
    % processing
    % --------------------------------------------------------------------------

    function [outLin, outSS] = Process(Batch_GWPE, in)
    % process all time blocks in the input
    %

        Nb = size(in,2);
    
        % input coefficients (F x Nb x M)
        X = in;

        % output coefficients
        % output is in format F x Nb x M
        Dlin = zeros( size(X) );
        Dss  = zeros( size(X) );

        % processing each frequency
        for f=1:Batch_GWPE.F

            % ------------------------------------------------------------------
            % construct the MC convolution matrix
            % ------------------------------------------------------------------

            X_f = squeeze( X(f,:,:) );
            
            % construct the convolution matrix
            XX_f = zeros(Nb, Batch_GWPE.M * Batch_GWPE.Lg);
            for m=1:Batch_GWPE.M
                tmp = convmtx( [ zeros( Batch_GWPE.tau, 1 ) ; squeeze(X(f,:,m)).' ] , Batch_GWPE.Lg );
                tmp = tmp(1:Nb,:);
                XX_f( 1:Nb , (m-1)*Batch_GWPE.Lg+1 : m*Batch_GWPE.Lg ) = tmp;
            end
            
            % ------------------------------------------------------------------
            % estimate late reverberant power
            % ------------------------------------------------------------------
            % use Lebart's model with the given RT60 to get an estimate of the
            % late reverberant power
            %
            % delay for the late reverberation is defined using the prediction
            % delay tau
            %
            % this is used only if the weights are computed using the PSD
            % estimates - if the weights are updated iteratively then this is
            % not required

            if any( strcmp(Batch_GWPE.weightType, {'rev','des'} ) )
                Batch_GWPE.updatePowerEstimators(X_f);
            end

            % ------------------------------------------------------------------
            % IRLS
            % ------------------------------------------------------------------
            
            % initial estimate
            Dlin_f = X_f;
            
            for it=1:Batch_GWPE.itMax
                
                % --------------------------------------------------------------
                % compute the weights
                % --------------------------------------------------------------

                w_f = Batch_GWPE.computeWeights( Dlin_f );

                % --------------------------------------------------------------
                % update prediction filters
                % --------------------------------------------------------------
                
                Q_f = XX_f' * diag( w_f ) * XX_f;
                R_f = XX_f' * diag( w_f ) * X_f;

                G_f = Q_f \ R_f;
                
                % --------------------------------------------------------------
                % dereverberation
                % --------------------------------------------------------------

                % estimate the reverberant component
                estRev_f = XX_f * G_f;

                % estimate the desired speech -> linear
                Dlin_f = X_f - estRev_f;
                
                % estimate the desired speech -> spectral subtraction
                Dss_f = sqrt( max(abs(X_f).^2 - abs(estRev_f).^2, 0) ) .* exp( 1j * angle( X_f ) );

            end
            
            Dlin(f,:,:) = Dlin_f;
            Dss (f,:,:) = Dss_f;
            
            % save for debugging
            % ------------------------------------------------------------------
            Batch_GWPE.debug{1}(f,:,:) = Batch_GWPE.revPowSmooth;
            Batch_GWPE.debug{2}(f,:,:) = Batch_GWPE.revDelayedPowSmooth;
            Batch_GWPE.debug{3}(f,:,:) = Batch_GWPE.desPowSmooth;
            Batch_GWPE.debug{4}(f,:,:) = Batch_GWPE.lateRevPow;
            
        end
        
        % output
        outLin = Dlin;
        outSS  = Dss;

    end

end % end methods


end % end classdef
