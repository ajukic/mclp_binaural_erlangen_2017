/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * vad_s.c
 *
 * Code generation for function 'vad_s'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "vad_s.h"
#include "mpower.h"
#include "vad_s_emxutil.h"
#include "indexShapeCheck.h"
#include "eml_int_forloop_overflow_check.h"
#include "power.h"
#include "abs.h"
#include "vad_s_data.h"

/* Variable Definitions */
static emlrtRSInfo emlrtRSI = { 13, "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m"
};

static emlrtRSInfo b_emlrtRSI = { 14, "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m"
};

static emlrtRSInfo c_emlrtRSI = { 16, "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m"
};

static emlrtRSInfo d_emlrtRSI = { 21, "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m"
};

static emlrtRSInfo e_emlrtRSI = { 23, "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m"
};

static emlrtRSInfo f_emlrtRSI = { 25, "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m"
};

static emlrtRSInfo g_emlrtRSI = { 27, "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m"
};

static emlrtRSInfo h_emlrtRSI = { 30, "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m"
};

static emlrtRSInfo s_emlrtRSI = { 16, "max",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/lib/matlab/datafun/max.m" };

static emlrtRSInfo t_emlrtRSI = { 18, "minOrMax",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/eml/+coder/+internal/minOrMax.m"
};

static emlrtRSInfo u_emlrtRSI = { 97, "minOrMax",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/eml/+coder/+internal/minOrMax.m"
};

static emlrtRSInfo v_emlrtRSI = { 281, "minOrMax",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/eml/+coder/+internal/minOrMax.m"
};

static emlrtRSInfo w_emlrtRSI = { 308, "minOrMax",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/eml/+coder/+internal/minOrMax.m"
};

static emlrtRSInfo y_emlrtRSI = { 44, "find",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/lib/matlab/elmat/find.m" };

static emlrtRSInfo ab_emlrtRSI = { 234, "find",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/lib/matlab/elmat/find.m" };

static emlrtRSInfo bb_emlrtRSI = { 253, "find",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/lib/matlab/elmat/find.m" };

static emlrtRTEInfo emlrtRTEI = { 1, 18, "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m"
};

static emlrtRTEInfo b_emlrtRTEI = { 253, 13, "find",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/lib/matlab/elmat/find.m" };

static emlrtRTEInfo c_emlrtRTEI = { 13, 1, "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m"
};

static emlrtRTEInfo d_emlrtRTEI = { 16, 1, "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m"
};

static emlrtRTEInfo e_emlrtRTEI = { 36, 6, "find",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/lib/matlab/elmat/find.m" };

static emlrtBCInfo emlrtBCI = { -1, -1, 14, 8, "idx", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtBCInfo b_emlrtBCI = { -1, -1, 14, 19, "idx", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtECInfo emlrtECI = { -1, 14, 8, "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m"
};

static emlrtECInfo b_emlrtECI = { 2, 16, 8, "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m"
};

static emlrtBCInfo c_emlrtBCI = { -1, -1, 21, 19, "idx", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtBCInfo d_emlrtBCI = { -1, -1, 21, 26, "idx2", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtBCInfo e_emlrtBCI = { -1, -1, 21, 17, "x", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtRTEInfo i_emlrtRTEI = { 22, 9, "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m"
};

static emlrtBCInfo f_emlrtBCI = { -1, -1, 23, 32, "idx2", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtBCInfo g_emlrtBCI = { -1, -1, 23, 41, "idx2", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtBCInfo h_emlrtBCI = { -1, -1, 23, 30, "x", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtBCInfo i_emlrtBCI = { -1, -1, 25, 28, "idx2", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtBCInfo j_emlrtBCI = { -1, -1, 25, 38, "idx", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtBCInfo k_emlrtBCI = { -1, -1, 25, 26, "x", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtBCInfo l_emlrtBCI = { -1, -1, 27, 20, "idx", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtBCInfo m_emlrtBCI = { -1, -1, 27, 27, "idx2", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtBCInfo n_emlrtBCI = { -1, -1, 27, 18, "x", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtBCInfo o_emlrtBCI = { -1, -1, 27, 40, "idx2", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtBCInfo p_emlrtBCI = { -1, -1, 27, 48, "idx", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtBCInfo q_emlrtBCI = { -1, -1, 27, 38, "x", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtBCInfo r_emlrtBCI = { -1, -1, 30, 15, "idx", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtBCInfo s_emlrtBCI = { -1, -1, 30, 22, "idx", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtBCInfo t_emlrtBCI = { -1, -1, 30, 13, "x", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtBCInfo u_emlrtBCI = { -1, -1, 16, 9, "idx", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

static emlrtRTEInfo j_emlrtRTEI = { 38, 19, "minOrMax",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/eml/+coder/+internal/minOrMax.m"
};

static emlrtRTEInfo k_emlrtRTEI = { 81, 19, "minOrMax",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/eml/+coder/+internal/minOrMax.m"
};

static emlrtRTEInfo l_emlrtRTEI = { 243, 9, "find",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/lib/matlab/elmat/find.m" };

static emlrtBCInfo v_emlrtBCI = { -1, -1, 16, 28, "idx", "vad_s",
  "/Volumes/MacHD_Files/Work/MATLAB/work/mclp_constr_SPL_2016/helper/SRMRToolbox-master/libs/vad/vad_s.m",
  0 };

/* Function Definitions */
void vad_s(const emlrtStack *sp, const emxArray_real_T *x, real_T fs,
           emxArray_real_T *x_new)
{
  emxArray_real_T *varargin_1;
  boolean_T overflow;
  int32_T ixstart;
  int32_T n;
  real_T mtmp;
  int32_T ix;
  boolean_T exitg3;
  emxArray_boolean_T *b_x;
  int32_T i0;
  emxArray_int32_T *ii;
  int32_T b_ii;
  boolean_T exitg2;
  boolean_T guard2 = false;
  int32_T iv0[2];
  int32_T i1;
  emxArray_real_T *idx;
  int32_T i2;
  int32_T iv1[2];
  int32_T i3;
  int32_T iv2[2];
  int32_T trueCount;
  boolean_T exitg1;
  boolean_T guard1 = false;
  int32_T iv3[2];
  emxArray_real_T *idx2;
  int32_T iv4[2];
  int32_T c_ii;
  int32_T iv5[2];
  int32_T iv6[2];
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack f_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  f_st.prev = &e_st;
  f_st.tls = e_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  emxInit_real_T(sp, &varargin_1, 1, &emlrtRTEI, true);

  /*  a simple Voice Activity Detection program for speech processing */
  /*  an energy threshold is used to remove the silence parts of speech signal */
  /*  the speech is assumed to be clean */
  st.site = &emlrtRSI;
  b_abs(&st, x, varargin_1);
  st.site = &emlrtRSI;
  b_st.site = &s_emlrtRSI;
  c_st.site = &t_emlrtRSI;
  if ((varargin_1->size[0] == 1) || (varargin_1->size[0] != 1)) {
    overflow = true;
  } else {
    overflow = false;
  }

  if (overflow) {
  } else {
    emlrtErrorWithMessageIdR2012b(&c_st, &j_emlrtRTEI,
      "Coder:toolbox:autoDimIncompatibility", 0);
  }

  if (varargin_1->size[0] > 0) {
  } else {
    emlrtErrorWithMessageIdR2012b(&c_st, &k_emlrtRTEI,
      "Coder:toolbox:eml_min_or_max_varDimZero", 0);
  }

  d_st.site = &u_emlrtRSI;
  ixstart = 1;
  n = varargin_1->size[0];
  mtmp = varargin_1->data[0];
  if (varargin_1->size[0] > 1) {
    if (muDoubleScalarIsNaN(varargin_1->data[0])) {
      e_st.site = &v_emlrtRSI;
      overflow = (varargin_1->size[0] > 2147483646);
      if (overflow) {
        f_st.site = &p_emlrtRSI;
        check_forloop_overflow_error(&f_st, true);
      }

      ix = 2;
      exitg3 = false;
      while ((!exitg3) && (ix <= n)) {
        ixstart = ix;
        if (!muDoubleScalarIsNaN(varargin_1->data[ix - 1])) {
          mtmp = varargin_1->data[ix - 1];
          exitg3 = true;
        } else {
          ix++;
        }
      }
    }

    if (ixstart < varargin_1->size[0]) {
      e_st.site = &w_emlrtRSI;
      if (ixstart + 1 > varargin_1->size[0]) {
        overflow = false;
      } else {
        overflow = (varargin_1->size[0] > 2147483646);
      }

      if (overflow) {
        f_st.site = &p_emlrtRSI;
        check_forloop_overflow_error(&f_st, true);
      }

      while (ixstart + 1 <= n) {
        if (varargin_1->data[ixstart] > mtmp) {
          mtmp = varargin_1->data[ixstart];
        }

        ixstart++;
      }
    }
  }

  emxInit_boolean_T(&d_st, &b_x, 1, &emlrtRTEI, true);
  st.site = &emlrtRSI;
  mtmp = mpower(mtmp);
  mtmp /= 100000.0;
  st.site = &emlrtRSI;
  b_st.site = &emlrtRSI;
  power(&b_st, x, varargin_1);
  i0 = b_x->size[0];
  b_x->size[0] = varargin_1->size[0];
  emxEnsureCapacity(&st, (emxArray__common *)b_x, i0, (int32_T)sizeof(boolean_T),
                    &emlrtRTEI);
  ix = varargin_1->size[0];
  for (i0 = 0; i0 < ix; i0++) {
    b_x->data[i0] = (varargin_1->data[i0] > mtmp);
  }

  emxInit_int32_T(&st, &ii, 1, &e_emlrtRTEI, true);
  b_st.site = &y_emlrtRSI;
  ixstart = b_x->size[0];
  n = 0;
  i0 = ii->size[0];
  ii->size[0] = b_x->size[0];
  emxEnsureCapacity(&b_st, (emxArray__common *)ii, i0, (int32_T)sizeof(int32_T),
                    &emlrtRTEI);
  c_st.site = &ab_emlrtRSI;
  if (1 > b_x->size[0]) {
    overflow = false;
  } else {
    overflow = (b_x->size[0] > 2147483646);
  }

  if (overflow) {
    d_st.site = &p_emlrtRSI;
    check_forloop_overflow_error(&d_st, true);
  }

  b_ii = 1;
  exitg2 = false;
  while ((!exitg2) && (b_ii <= ixstart)) {
    guard2 = false;
    if (b_x->data[b_ii - 1]) {
      n++;
      ii->data[n - 1] = b_ii;
      if (n >= ixstart) {
        exitg2 = true;
      } else {
        guard2 = true;
      }
    } else {
      guard2 = true;
    }

    if (guard2) {
      b_ii++;
    }
  }

  if (n <= b_x->size[0]) {
  } else {
    emlrtErrorWithMessageIdR2012b(&b_st, &l_emlrtRTEI,
      "Coder:builtins:AssertionFailed", 0);
  }

  if (b_x->size[0] == 1) {
    if (n == 0) {
      i0 = ii->size[0];
      ii->size[0] = 0;
      emxEnsureCapacity(&b_st, (emxArray__common *)ii, i0, (int32_T)sizeof
                        (int32_T), &emlrtRTEI);
    }
  } else {
    if (1 > n) {
      i0 = 0;
    } else {
      i0 = n;
    }

    iv0[0] = 1;
    iv0[1] = i0;
    c_st.site = &bb_emlrtRSI;
    indexShapeCheck(&c_st, ii->size[0], iv0);
    i1 = ii->size[0];
    ii->size[0] = i0;
    emxEnsureCapacity(&b_st, (emxArray__common *)ii, i1, (int32_T)sizeof(int32_T),
                      &b_emlrtRTEI);
  }

  emxInit_real_T(&b_st, &idx, 1, &c_emlrtRTEI, true);
  i0 = idx->size[0];
  idx->size[0] = ii->size[0];
  emxEnsureCapacity(&st, (emxArray__common *)idx, i0, (int32_T)sizeof(real_T),
                    &emlrtRTEI);
  ix = ii->size[0];
  for (i0 = 0; i0 < ix; i0++) {
    idx->data[i0] = ii->data[i0];
  }

  /*  or 30db? */
  if (2 > idx->size[0]) {
    i0 = 0;
    i2 = 0;
  } else {
    i0 = 1;
    i1 = idx->size[0];
    i2 = idx->size[0];
    if (!((i2 >= 1) && (i2 <= i1))) {
      emlrtDynamicBoundsCheckR2012b(i2, 1, i1, &emlrtBCI, sp);
    }
  }

  iv1[0] = 1;
  iv1[1] = i2 - i0;
  st.site = &b_emlrtRSI;
  indexShapeCheck(&st, idx->size[0], iv1);
  if (1 > idx->size[0] - 1) {
    i3 = 0;
  } else {
    i1 = idx->size[0];
    if (!(1 <= i1)) {
      emlrtDynamicBoundsCheckR2012b(1, 1, i1, &b_emlrtBCI, sp);
    }

    i1 = idx->size[0];
    i3 = idx->size[0] - 1;
    if (!((i3 >= 1) && (i3 <= i1))) {
      emlrtDynamicBoundsCheckR2012b(i3, 1, i1, &b_emlrtBCI, sp);
    }
  }

  iv2[0] = 1;
  iv2[1] = i3;
  st.site = &b_emlrtRSI;
  indexShapeCheck(&st, idx->size[0], iv2);
  i1 = i2 - i0;
  if (i1 != i3) {
    emlrtSizeEqCheck1DR2012b(i1, i3, &emlrtECI, sp);
  }

  /*  set threshold to 50ms */
  n = i2 - i0;
  for (ix = 0; ix < n; ix++) {
    if (idx->data[i0 + ix] - idx->data[ix] > 0.05 * fs) {
      i1 = idx->size[0];
      if (!((ix + 1 >= 1) && (ix + 1 <= i1))) {
        emlrtDynamicBoundsCheckR2012b(ix + 1, 1, i1, &u_emlrtBCI, sp);
      }
    }
  }

  n = i2 - i0;
  trueCount = 0;
  for (ix = 0; ix < n; ix++) {
    if (idx->data[i0 + ix] - idx->data[ix] > 0.05 * fs) {
      trueCount++;
    }
  }

  st.site = &c_emlrtRSI;
  mtmp = 0.05 * fs;
  i1 = b_x->size[0];
  b_x->size[0] = i2 - i0;
  emxEnsureCapacity(&st, (emxArray__common *)b_x, i1, (int32_T)sizeof(boolean_T),
                    &emlrtRTEI);
  ix = i2 - i0;
  for (i1 = 0; i1 < ix; i1++) {
    b_x->data[i1] = (idx->data[i0 + i1] - idx->data[i1] > mtmp);
  }

  b_st.site = &y_emlrtRSI;
  ixstart = b_x->size[0];
  n = 0;
  i1 = ii->size[0];
  ii->size[0] = b_x->size[0];
  emxEnsureCapacity(&b_st, (emxArray__common *)ii, i1, (int32_T)sizeof(int32_T),
                    &emlrtRTEI);
  c_st.site = &ab_emlrtRSI;
  b_ii = 1;
  exitg1 = false;
  while ((!exitg1) && (b_ii <= ixstart)) {
    guard1 = false;
    if (b_x->data[b_ii - 1]) {
      n++;
      ii->data[n - 1] = b_ii;
      if (n >= ixstart) {
        exitg1 = true;
      } else {
        guard1 = true;
      }
    } else {
      guard1 = true;
    }

    if (guard1) {
      b_ii++;
    }
  }

  if (n <= b_x->size[0]) {
  } else {
    emlrtErrorWithMessageIdR2012b(&b_st, &l_emlrtRTEI,
      "Coder:builtins:AssertionFailed", 0);
  }

  if (b_x->size[0] == 1) {
    if (n == 0) {
      i1 = ii->size[0];
      ii->size[0] = 0;
      emxEnsureCapacity(&b_st, (emxArray__common *)ii, i1, (int32_T)sizeof
                        (int32_T), &emlrtRTEI);
    }
  } else {
    if (1 > n) {
      i1 = 0;
    } else {
      i1 = n;
    }

    iv3[0] = 1;
    iv3[1] = i1;
    c_st.site = &bb_emlrtRSI;
    indexShapeCheck(&c_st, ii->size[0], iv3);
    i3 = ii->size[0];
    ii->size[0] = i1;
    emxEnsureCapacity(&b_st, (emxArray__common *)ii, i3, (int32_T)sizeof(int32_T),
                      &b_emlrtRTEI);
  }

  emxFree_boolean_T(&b_x);
  n = idx->size[0];
  i1 = varargin_1->size[0];
  varargin_1->size[0] = ii->size[0];
  emxEnsureCapacity(sp, (emxArray__common *)varargin_1, i1, (int32_T)sizeof
                    (real_T), &emlrtRTEI);
  ix = ii->size[0];
  for (i1 = 0; i1 < ix; i1++) {
    i3 = ii->data[i1] + 1;
    if (!((i3 >= 1) && (i3 <= n))) {
      emlrtDynamicBoundsCheckR2012b(i3, 1, n, &v_emlrtBCI, sp);
    }

    varargin_1->data[i1] = idx->data[i3 - 1];
  }

  i1 = ii->size[0];
  if (trueCount != i1) {
    emlrtDimSizeEqCheckR2012b(trueCount, i1, &b_emlrtECI, sp);
  }

  n = (i2 - i0) - 1;
  trueCount = 0;
  for (ix = 0; ix <= n; ix++) {
    if (idx->data[i0 + ix] - idx->data[ix] > 0.05 * fs) {
      trueCount++;
    }
  }

  i1 = ii->size[0];
  ii->size[0] = trueCount;
  emxEnsureCapacity(sp, (emxArray__common *)ii, i1, (int32_T)sizeof(int32_T),
                    &emlrtRTEI);
  ixstart = 0;
  for (ix = 0; ix <= n; ix++) {
    if (idx->data[i0 + ix] - idx->data[ix] > 0.05 * fs) {
      ii->data[ixstart] = ix + 1;
      ixstart++;
    }
  }

  emxInit_real_T1(sp, &idx2, 2, &d_emlrtRTEI, true);
  i0 = idx2->size[0] * idx2->size[1];
  idx2->size[0] = 2;
  idx2->size[1] = ii->size[0];
  emxEnsureCapacity(sp, (emxArray__common *)idx2, i0, (int32_T)sizeof(real_T),
                    &emlrtRTEI);
  ix = ii->size[0];
  for (i0 = 0; i0 < ix; i0++) {
    idx2->data[idx2->size[0] * i0] = idx->data[ii->data[i0] - 1];
  }

  emxFree_int32_T(&ii);
  ix = varargin_1->size[0];
  for (i0 = 0; i0 < ix; i0++) {
    idx2->data[1 + idx2->size[0] * i0] = varargin_1->data[i0];
  }

  emxFree_real_T(&varargin_1);
  n = idx2->size[1] << 1;
  if (!(n == 0)) {
    n = idx2->size[1] << 1;
    if (n > 2) {
      i0 = idx->size[0];
      if (!(1 <= i0)) {
        emlrtDynamicBoundsCheckR2012b(1, 1, i0, &c_emlrtBCI, sp);
      }

      n = idx2->size[1] << 1;
      if (!(1 <= n)) {
        emlrtDynamicBoundsCheckR2012b(1, 1, n, &d_emlrtBCI, sp);
      }

      if (idx->data[0] > idx2->data[0]) {
        i1 = 1;
        i0 = 1;
      } else {
        i0 = x->size[0];
        i1 = (int32_T)idx->data[0];
        if (!((i1 >= 1) && (i1 <= i0))) {
          emlrtDynamicBoundsCheckR2012b(i1, 1, i0, &e_emlrtBCI, sp);
        }

        i0 = x->size[0];
        i2 = (int32_T)idx2->data[0];
        if (!((i2 >= 1) && (i2 <= i0))) {
          emlrtDynamicBoundsCheckR2012b(i2, 1, i0, &e_emlrtBCI, sp);
        }

        i0 = i2 + 1;
      }

      iv4[0] = 1;
      iv4[1] = i0 - i1;
      st.site = &d_emlrtRSI;
      indexShapeCheck(&st, x->size[0], iv4);
      i2 = x_new->size[0];
      x_new->size[0] = i0 - i1;
      emxEnsureCapacity(sp, (emxArray__common *)x_new, i2, (int32_T)sizeof
                        (real_T), &emlrtRTEI);
      ix = i0 - i1;
      for (i0 = 0; i0 < ix; i0++) {
        x_new->data[i0] = x->data[(i1 + i0) - 1];
      }

      n = idx2->size[1] << 1;
      i0 = (int32_T)(((real_T)n - 2.0) / 2.0);
      n = idx2->size[1] << 1;
      emlrtForLoopVectorCheckR2012b(2.0, 2.0, (real_T)n - 2.0, mxDOUBLE_CLASS,
        i0, &i_emlrtRTEI, sp);
      b_ii = 0;
      while (b_ii <= i0 - 1) {
        c_ii = 2 + (b_ii << 1);
        n = idx2->size[1] << 1;
        i1 = c_ii;
        if (!(i1 <= n)) {
          emlrtDynamicBoundsCheckR2012b(i1, 1, n, &f_emlrtBCI, sp);
        }

        n = idx2->size[1] << 1;
        i1 = c_ii + 1;
        if (!(i1 <= n)) {
          emlrtDynamicBoundsCheckR2012b(i1, 1, n, &g_emlrtBCI, sp);
        }

        if ((int32_T)idx2->data[c_ii - 1] > (int32_T)idx2->data[c_ii]) {
          i2 = 1;
          i1 = 1;
        } else {
          i1 = x->size[0];
          i2 = (int32_T)idx2->data[c_ii - 1];
          if (!((i2 >= 1) && (i2 <= i1))) {
            emlrtDynamicBoundsCheckR2012b(i2, 1, i1, &h_emlrtBCI, sp);
          }

          i1 = x->size[0];
          i3 = (int32_T)idx2->data[c_ii];
          if (!((i3 >= 1) && (i3 <= i1))) {
            emlrtDynamicBoundsCheckR2012b(i3, 1, i1, &h_emlrtBCI, sp);
          }

          i1 = i3 + 1;
        }

        iv5[0] = 1;
        iv5[1] = i1 - i2;
        st.site = &e_emlrtRSI;
        indexShapeCheck(&st, x->size[0], iv5);
        ixstart = x_new->size[0];
        i3 = x_new->size[0];
        x_new->size[0] = (ixstart + i1) - i2;
        emxEnsureCapacity(sp, (emxArray__common *)x_new, i3, (int32_T)sizeof
                          (real_T), &emlrtRTEI);
        ix = i1 - i2;
        for (i1 = 0; i1 < ix; i1++) {
          x_new->data[ixstart + i1] = x->data[(i2 + i1) - 1];
        }

        b_ii++;
        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }
      }

      n = idx2->size[1] << 1;
      ixstart = idx2->size[1] << 1;
      if (!((n >= 1) && (n <= ixstart))) {
        emlrtDynamicBoundsCheckR2012b(n, 1, ixstart, &i_emlrtBCI, sp);
      }

      i0 = idx->size[0];
      i1 = idx->size[0];
      if (!((i1 >= 1) && (i1 <= i0))) {
        emlrtDynamicBoundsCheckR2012b(i1, 1, i0, &j_emlrtBCI, sp);
      }

      n = idx2->size[1] << 1;
      if (idx2->data[n - 1] > idx->data[idx->size[0] - 1]) {
        i1 = 1;
        i0 = 1;
      } else {
        n = idx2->size[1] << 1;
        i0 = x->size[0];
        i1 = (int32_T)idx2->data[n - 1];
        if (!((i1 >= 1) && (i1 <= i0))) {
          emlrtDynamicBoundsCheckR2012b(i1, 1, i0, &k_emlrtBCI, sp);
        }

        i0 = x->size[0];
        i2 = (int32_T)idx->data[idx->size[0] - 1];
        if (!((i2 >= 1) && (i2 <= i0))) {
          emlrtDynamicBoundsCheckR2012b(i2, 1, i0, &k_emlrtBCI, sp);
        }

        i0 = i2 + 1;
      }

      iv6[0] = 1;
      iv6[1] = i0 - i1;
      st.site = &f_emlrtRSI;
      indexShapeCheck(&st, x->size[0], iv6);
      ixstart = x_new->size[0];
      i2 = x_new->size[0];
      x_new->size[0] = (ixstart + i0) - i1;
      emxEnsureCapacity(sp, (emxArray__common *)x_new, i2, (int32_T)sizeof
                        (real_T), &emlrtRTEI);
      ix = i0 - i1;
      for (i0 = 0; i0 < ix; i0++) {
        x_new->data[ixstart + i0] = x->data[(i1 + i0) - 1];
      }
    } else {
      i0 = idx->size[0];
      if (!(1 <= i0)) {
        emlrtDynamicBoundsCheckR2012b(1, 1, i0, &l_emlrtBCI, sp);
      }

      n = idx2->size[1] << 1;
      if (!(1 <= n)) {
        emlrtDynamicBoundsCheckR2012b(1, 1, n, &m_emlrtBCI, sp);
      }

      if (idx->data[0] > idx2->data[0]) {
        i1 = 1;
        i0 = 1;
      } else {
        i0 = x->size[0];
        i1 = (int32_T)idx->data[0];
        if (!((i1 >= 1) && (i1 <= i0))) {
          emlrtDynamicBoundsCheckR2012b(i1, 1, i0, &n_emlrtBCI, sp);
        }

        i0 = x->size[0];
        i2 = (int32_T)idx2->data[0];
        if (!((i2 >= 1) && (i2 <= i0))) {
          emlrtDynamicBoundsCheckR2012b(i2, 1, i0, &n_emlrtBCI, sp);
        }

        i0 = i2 + 1;
      }

      iv4[0] = 1;
      iv4[1] = i0 - i1;
      st.site = &g_emlrtRSI;
      indexShapeCheck(&st, x->size[0], iv4);
      n = idx2->size[1] << 1;
      if (!(1 <= n)) {
        emlrtDynamicBoundsCheckR2012b(1, 1, n, &o_emlrtBCI, sp);
      }

      i2 = idx->size[0];
      i3 = idx->size[0];
      if (!((i3 >= 1) && (i3 <= i2))) {
        emlrtDynamicBoundsCheckR2012b(i3, 1, i2, &p_emlrtBCI, sp);
      }

      if (idx2->data[0] > idx->data[idx->size[0] - 1]) {
        i3 = 1;
        i2 = 1;
      } else {
        i2 = x->size[0];
        i3 = (int32_T)idx2->data[0];
        if (!((i3 >= 1) && (i3 <= i2))) {
          emlrtDynamicBoundsCheckR2012b(i3, 1, i2, &q_emlrtBCI, sp);
        }

        i2 = x->size[0];
        ixstart = (int32_T)idx->data[idx->size[0] - 1];
        if (!((ixstart >= 1) && (ixstart <= i2))) {
          emlrtDynamicBoundsCheckR2012b(ixstart, 1, i2, &q_emlrtBCI, sp);
        }

        i2 = ixstart + 1;
      }

      iv6[0] = 1;
      iv6[1] = i2 - i3;
      st.site = &g_emlrtRSI;
      indexShapeCheck(&st, x->size[0], iv6);
      ixstart = x_new->size[0];
      x_new->size[0] = ((i0 - i1) + i2) - i3;
      emxEnsureCapacity(sp, (emxArray__common *)x_new, ixstart, (int32_T)sizeof
                        (real_T), &emlrtRTEI);
      ix = i0 - i1;
      for (ixstart = 0; ixstart < ix; ixstart++) {
        x_new->data[ixstart] = x->data[(i1 + ixstart) - 1];
      }

      ix = i2 - i3;
      for (i2 = 0; i2 < ix; i2++) {
        x_new->data[(i2 + i0) - i1] = x->data[(i3 + i2) - 1];
      }
    }
  } else {
    i0 = idx->size[0];
    if (!(1 <= i0)) {
      emlrtDynamicBoundsCheckR2012b(1, 1, i0, &r_emlrtBCI, sp);
    }

    i0 = idx->size[0];
    i1 = idx->size[0];
    if (!((i1 >= 1) && (i1 <= i0))) {
      emlrtDynamicBoundsCheckR2012b(i1, 1, i0, &s_emlrtBCI, sp);
    }

    if (idx->data[0] > idx->data[idx->size[0] - 1]) {
      i1 = 1;
      i0 = 1;
    } else {
      i0 = x->size[0];
      i1 = (int32_T)idx->data[0];
      if (!((i1 >= 1) && (i1 <= i0))) {
        emlrtDynamicBoundsCheckR2012b(i1, 1, i0, &t_emlrtBCI, sp);
      }

      i0 = x->size[0];
      i2 = (int32_T)idx->data[idx->size[0] - 1];
      if (!((i2 >= 1) && (i2 <= i0))) {
        emlrtDynamicBoundsCheckR2012b(i2, 1, i0, &t_emlrtBCI, sp);
      }

      i0 = i2 + 1;
    }

    iv4[0] = 1;
    iv4[1] = i0 - i1;
    st.site = &h_emlrtRSI;
    indexShapeCheck(&st, x->size[0], iv4);
    i2 = x_new->size[0];
    x_new->size[0] = i0 - i1;
    emxEnsureCapacity(sp, (emxArray__common *)x_new, i2, (int32_T)sizeof(real_T),
                      &emlrtRTEI);
    ix = i0 - i1;
    for (i0 = 0; i0 < ix; i0++) {
      x_new->data[i0] = x->data[(i1 + i0) - 1];
    }
  }

  emxFree_real_T(&idx2);
  emxFree_real_T(&idx);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (vad_s.c) */
