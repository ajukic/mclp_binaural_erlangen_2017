/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * vad_s_initialize.h
 *
 * Code generation for function 'vad_s_initialize'
 *
 */

#ifndef __VAD_S_INITIALIZE_H__
#define __VAD_S_INITIALIZE_H__

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "vad_s_types.h"

/* Function Declarations */
extern void vad_s_initialize(void);

#endif

/* End of code generation (vad_s_initialize.h) */
