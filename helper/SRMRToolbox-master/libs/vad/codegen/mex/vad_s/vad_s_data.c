/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * vad_s_data.c
 *
 * Code generation for function 'vad_s_data'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "vad_s.h"
#include "vad_s_data.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
const volatile char_T *emlrtBreakCheckR2012bFlagVar = NULL;
emlrtContext emlrtContextGlobal = { true, false, 131419U, NULL, "vad_s", NULL,
  false, { 2045744189U, 2170104910U, 2743257031U, 4284093946U }, NULL };

emlrtRSInfo o_emlrtRSI = { 19, "scalexpAllocNoCheck",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/eml/+coder/+internal/scalexpAllocNoCheck.m"
};

emlrtRSInfo p_emlrtRSI = { 20, "eml_int_forloop_overflow_check",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"
};

emlrtRSInfo x_emlrtRSI = { 37, "mpower",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/lib/matlab/ops/mpower.m" };

/* End of code generation (vad_s_data.c) */
