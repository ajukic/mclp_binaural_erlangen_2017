/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * power.c
 *
 * Code generation for function 'power'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "vad_s.h"
#include "power.h"
#include "vad_s_emxutil.h"
#include "eml_int_forloop_overflow_check.h"
#include "scalexpAlloc.h"
#include "vad_s_data.h"

/* Variable Definitions */
static emlrtRSInfo i_emlrtRSI = { 49, "power",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/lib/matlab/ops/power.m" };

static emlrtRSInfo j_emlrtRSI = { 58, "power",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/lib/matlab/ops/power.m" };

static emlrtRSInfo k_emlrtRSI = { 60, "power",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/lib/matlab/ops/power.m" };

static emlrtRSInfo l_emlrtRSI = { 73, "applyScalarFunction",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/eml/+coder/+internal/applyScalarFunction.m"
};

static emlrtRSInfo m_emlrtRSI = { 132, "applyScalarFunction",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/eml/+coder/+internal/applyScalarFunction.m"
};

static emlrtRSInfo n_emlrtRSI = { 16, "scalexpAlloc",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/eml/+coder/+internal/scalexpAlloc.m"
};

static emlrtRTEInfo f_emlrtRTEI = { 1, 14, "power",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/lib/matlab/ops/power.m" };

static emlrtRTEInfo m_emlrtRTEI = { 17, 19, "scalexpAlloc",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/eml/+coder/+internal/scalexpAlloc.m"
};

/* Function Definitions */
void power(const emlrtStack *sp, const emxArray_real_T *a, emxArray_real_T *y)
{
  uint32_T unnamed_idx_0;
  int32_T k;
  boolean_T overflow;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &i_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  b_st.site = &j_emlrtRSI;
  c_st.site = &l_emlrtRSI;
  d_st.site = &n_emlrtRSI;
  unnamed_idx_0 = (uint32_T)a->size[0];
  k = y->size[0];
  y->size[0] = (int32_T)unnamed_idx_0;
  emxEnsureCapacity(&d_st, (emxArray__common *)y, k, (int32_T)sizeof(real_T),
                    &f_emlrtRTEI);
  if (dimagree(y, a)) {
  } else {
    emlrtErrorWithMessageIdR2012b(&c_st, &m_emlrtRTEI, "MATLAB:dimagree", 0);
  }

  c_st.site = &m_emlrtRSI;
  if (1 > a->size[0]) {
    overflow = false;
  } else {
    overflow = (a->size[0] > 2147483646);
  }

  if (overflow) {
    d_st.site = &p_emlrtRSI;
    check_forloop_overflow_error(&d_st, true);
  }

  for (k = 0; k + 1 <= a->size[0]; k++) {
    y->data[k] = a->data[k] * a->data[k];
  }

  b_st.site = &k_emlrtRSI;
}

/* End of code generation (power.c) */
