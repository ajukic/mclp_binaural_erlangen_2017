/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_vad_s_api.h
 *
 * Code generation for function '_coder_vad_s_api'
 *
 */

#ifndef ___CODER_VAD_S_API_H__
#define ___CODER_VAD_S_API_H__

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "vad_s_types.h"

/* Function Declarations */
extern void vad_s_api(const mxArray * const prhs[2], const mxArray *plhs[1]);

#endif

/* End of code generation (_coder_vad_s_api.h) */
