/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * vad_s_initialize.c
 *
 * Code generation for function 'vad_s_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "vad_s.h"
#include "vad_s_initialize.h"
#include "_coder_vad_s_mex.h"
#include "vad_s_data.h"

/* Function Definitions */
void vad_s_initialize(void)
{
  emlrtStack st = { NULL, NULL, NULL };

  mexFunctionCreateRootTLS();
  emlrtBreakCheckR2012bFlagVar = emlrtGetBreakCheckFlagAddressR2012b();
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  emlrtFirstTimeR2012b(emlrtRootTLSGlobal);
}

/* End of code generation (vad_s_initialize.c) */
