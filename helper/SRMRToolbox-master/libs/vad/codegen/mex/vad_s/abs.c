/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * abs.c
 *
 * Code generation for function 'abs'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "vad_s.h"
#include "abs.h"
#include "vad_s_emxutil.h"
#include "eml_int_forloop_overflow_check.h"
#include "vad_s_data.h"

/* Variable Definitions */
static emlrtRSInfo q_emlrtRSI = { 16, "abs",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/lib/matlab/elfun/abs.m" };

static emlrtRSInfo r_emlrtRSI = { 67, "applyScalarFunction",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/eml/+coder/+internal/applyScalarFunction.m"
};

static emlrtRTEInfo g_emlrtRTEI = { 1, 14, "abs",
  "/Applications/MATLAB_R2015b.app/toolbox/eml/lib/matlab/elfun/abs.m" };

/* Function Definitions */
void b_abs(const emlrtStack *sp, const emxArray_real_T *x, emxArray_real_T *y)
{
  uint32_T unnamed_idx_0;
  int32_T k;
  boolean_T overflow;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &q_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  unnamed_idx_0 = (uint32_T)x->size[0];
  k = y->size[0];
  y->size[0] = (int32_T)unnamed_idx_0;
  emxEnsureCapacity(&st, (emxArray__common *)y, k, (int32_T)sizeof(real_T),
                    &g_emlrtRTEI);
  b_st.site = &r_emlrtRSI;
  if (1 > x->size[0]) {
    overflow = false;
  } else {
    overflow = (x->size[0] > 2147483646);
  }

  if (overflow) {
    c_st.site = &p_emlrtRSI;
    check_forloop_overflow_error(&c_st, true);
  }

  for (k = 0; k + 1 <= x->size[0]; k++) {
    y->data[k] = muDoubleScalarAbs(x->data[k]);
  }
}

/* End of code generation (abs.c) */
