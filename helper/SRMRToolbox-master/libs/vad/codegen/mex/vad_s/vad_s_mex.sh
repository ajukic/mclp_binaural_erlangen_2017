MATLAB="/Applications/MATLAB_R2015b.app"
Arch=maci64
ENTRYPOINT=mexFunction
MAPFILE=$ENTRYPOINT'.map'
PREFDIR="/Users/ante/.matlab/R2015b"
OPTSFILE_NAME="./setEnv.sh"
. $OPTSFILE_NAME
COMPILER=$CC
. $OPTSFILE_NAME
echo "# Make settings for vad_s" > vad_s_mex.mki
echo "CC=$CC" >> vad_s_mex.mki
echo "CFLAGS=$CFLAGS" >> vad_s_mex.mki
echo "CLIBS=$CLIBS" >> vad_s_mex.mki
echo "COPTIMFLAGS=$COPTIMFLAGS" >> vad_s_mex.mki
echo "CDEBUGFLAGS=$CDEBUGFLAGS" >> vad_s_mex.mki
echo "CXX=$CXX" >> vad_s_mex.mki
echo "CXXFLAGS=$CXXFLAGS" >> vad_s_mex.mki
echo "CXXLIBS=$CXXLIBS" >> vad_s_mex.mki
echo "CXXOPTIMFLAGS=$CXXOPTIMFLAGS" >> vad_s_mex.mki
echo "CXXDEBUGFLAGS=$CXXDEBUGFLAGS" >> vad_s_mex.mki
echo "LD=$LD" >> vad_s_mex.mki
echo "LDFLAGS=$LDFLAGS" >> vad_s_mex.mki
echo "LDOPTIMFLAGS=$LDOPTIMFLAGS" >> vad_s_mex.mki
echo "LDDEBUGFLAGS=$LDDEBUGFLAGS" >> vad_s_mex.mki
echo "Arch=$Arch" >> vad_s_mex.mki
echo OMPFLAGS= >> vad_s_mex.mki
echo OMPLINKFLAGS= >> vad_s_mex.mki
echo "EMC_COMPILER=Xcode with Clang" >> vad_s_mex.mki
echo "EMC_CONFIG=optim" >> vad_s_mex.mki
"/Applications/MATLAB_R2015b.app/bin/maci64/gmake" -B -f vad_s_mex.mk
