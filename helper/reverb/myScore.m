function score = myScore(degSig, refSig, fs, resFile, resDir, snrNorm)
%% SCORE_SIM
%% Evaluate speech enhancement results for SimData. This is
%% designed for use in the REVERB challenge. 
%%
%% Written and distributed by the REVERB challenge organizers on 1 July, 2013
%% Inquiries to the challenge organizers (REVERB-challenge@lab.ntt.co.jp)
% 
%
% SRMR and PESQ should be available
% - SRMR in Matlab path
% - PESQ in system path
%

% default normlization for fwsegsnr
%----------------------------------------------------------------------

if nargin<6 || isempty(snrNorm)
    snrNorm = 'rms';
end


% Set up
%----------------------------------------------------------------------

%% Cepstral distance
param_cd = struct('frame' , 0.025   , ...
		  'shift' , 0.01    , ...
		  'window', @hanning, ...
		  'order' , 24      , ...
		  'timdif', 0.0     , ...
		  'cmn'   , 'y');

%% Log likelihood ratio
param_llr = struct('frame' , 0.025, ...
		   'shift' , 0.01, ...
		   'window', @hanning, ...
		   'lpcorder', 12);

%% Frequency-weighted segmental SNR
param_fwsegsnr = struct('frame'  , 0.025, ...
			'shift'  , 0.01, ...
			'window' , @hanning, ...
			'numband', 23, ...
            'norm', snrNorm);

% Create a result file.
%----------------------------------------------------------------------

if nargin<5 || ( isempty(resFile) || isempty(resDir) )
    
    fid = [];
    
else
    
    resDir = fullfile(resDir);

    cmd = ['mkdir -p -v ', resDir];
    system(cmd);

    fid  = fopen(fullfile(resDir, resFile), 'w');
    
end

fids = [1, fid];

% Parameters
%----------------------------------------------------------------------

for m = 1 : length(fids)
  fprintf(fids(m), '%s\n', datestr(now, 'mmmm dd, yyyy  HH:MM:SS AM'));
  fprintf(fids(m), '%s\n\n', fullfile(pwd, mfilename));
  
%   fprintf(fids(m), 'Cepstrum parameters:\n');
%   fprintf(fids(m), 'FRAME SIZE     = %d ms\n', param_cd.frame * 1e3);
%   fprintf(fids(m), 'SHIFT SIZE     = %d ms\n', param_cd.shift * 1e3);
%   fprintf(fids(m), 'WINDOW         = %s\n'   , func2str(param_cd.window));
%   fprintf(fids(m), 'CEPS ORDER     = %d\n'   , param_cd.order);
%   fprintf(fids(m), 'MAX TIME DIFF  = %d ms\n', param_cd.timdif * 1e3);
%   fprintf(fids(m), 'MEAN NORMALIZE = %s\n\n' , param_cd.cmn);
%   
%   fprintf(fids(m), 'Log likelihood ratio parameters:\n');
%   fprintf(fids(m), 'FRAME SIZE     = %d ms\n', param_llr.frame * 1e3);
%   fprintf(fids(m), 'SHIFT SIZE     = %d ms\n', param_llr.shift * 1e3);
%   fprintf(fids(m), 'WINDOW         = %s\n'   , func2str(param_llr.window));
%   fprintf(fids(m), 'LPC ORDER     = %d\n\n'    , param_llr.lpcorder);
% 
%   fprintf(fids(m), 'Frequency-weighted segmental SNR parameters:\n');
%   fprintf(fids(m), 'FRAME SIZE     = %d ms\n', param_fwsegsnr.frame * 1e3);
%   fprintf(fids(m), 'SHIFT SIZE     = %d ms\n', param_fwsegsnr.shift * 1e3);
%   fprintf(fids(m), 'WINDOW         = %s\n'   , func2str(param_fwsegsnr.window));
%   fprintf(fids(m), 'MEL BANDS      = %s\n'   , num2str(param_fwsegsnr.numband));
  
  fprintf(fids(m), '----------------------------------------------------------------------\n');
  fprintf(fids(m), 'Evaluation result\n\n');
end

cd_mean   = 0;
cd_med    = 0;
srmr_mean = 0;
llr_mean  = 0;
llr_med   = 0;
snr_mean  = 0;
snr_med   = 0;
pesq_mean = 0;


%% Load signals.
y = degSig;
x = refSig;

if length(degSig) > length(refSig)
    degSig = degSig(1 : length(refSig));
elseif length(degSig) < length(refSig)
    refSig = refSig(1 : length(degSig));
else
    ;
end


%%%%%%%%%%%%%%%%%%%%%%%
%% Cepstral distance %%
%%%%%%%%%%%%%%%%%%%%%%%

[cd_mean, cd_med, cd_all] = cepsdist(degSig, refSig, fs, param_cd);

for m = 1 : length(fids)
    fprintf(fids(m), '\tCEPSDIST (MEAN): %6.2f dB\n', cd_mean);
    fprintf(fids(m), '\tCEPSDIST (MED) : %6.2f dB\n', cd_med);
end


%%%%%%%%%%
%% SRMR %%
%%%%%%%%%%

srmr_mean = SRMR(degSig, fs, 'fast', 1, 'norm', 1);

for m = 1 : length(fids)
    fprintf(fids(m), '\tSRMR           : %6.2f\n', srmr_mean);  
end


%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Log likelihood ratio %%
%%%%%%%%%%%%%%%%%%%%%%%%%%

[llr_mean, llr_med] = lpcllr(degSig, refSig, fs, param_llr);

for m = 1 : length(fids)
    fprintf(fids(m), '\tLLR      (MEAN): %6.2f\n', llr_mean);  
    fprintf(fids(m), '\tLLR      (MED) : %6.2f\n', llr_med);  
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Frequency-weighted segmental SNR %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[snr_mean, snr_med, snr_all] = fwsegsnr(degSig, refSig, fs, param_fwsegsnr);

for m = 1 : length(fids)
    fprintf(fids(m), '\tFWSEGSNR (MEAN): %6.2f dB\n', snr_mean);  
    fprintf(fids(m), '\tFWSEGSNR (MED) : %6.2f dB\n', snr_med);  
end
  
  
%%%%%%%%%%
%% PESQ %%
%%%%%%%%%%

try
    pesq_mean = calcpesq(degSig, refSig, fs);
catch
    % error
    pesq_mean = NaN;
end

% check if pesq is empty and set it to -Inf
if isempty(pesq_mean)
    pesq_mean = NaN;
end
    
    
for m = 1 : length(fids)
	fprintf(fids(m), '\tPESQ           : %6.2f\n', pesq_mean);  
end

for m = 1 : length(fids)
    fprintf(fids(m), '\n');
end

%% Score for output

score.cd_mean   = cd_mean;
score.cd_med    = cd_med;
score.cd_all    = cd_all;
score.srmr_mean = srmr_mean;
score.llr_mean  = llr_mean;
score.llr_med   = llr_med;
score.snr_mean  = snr_mean;
score.snr_med   = snr_med;
score.snr_all   = snr_all;
score.pesq_mean = pesq_mean;


%%

% % Summary is not required since this function processes only one pair.
% %
% % Print a summary.
% %----------------------------------------------------------------------
% 
% avg_cd_mean   = mean(cd_mean);
% avg_cd_med    = mean(cd_med);
% avg_srmr_mean = mean(srmr_mean);
% avg_llr_mean  = mean(llr_mean);
% avg_llr_med   = mean(llr_med);
% avg_snr_mean  = mean(snr_mean);
% avg_snr_med   = mean(snr_med);
% 
% avg_pesq_mean = mean(pesq_mean);
% 
% for m = 1 : length(fids)
%     fprintf(fids(m), '----------------------------------------------------------------------\n');
%     fprintf(fids(m), 'Summary\n\n');
%     fprintf(fids(m), 'AVG CEPSDIST (MEAN): %6.2f dB\n', avg_cd_mean);
%     fprintf(fids(m), 'AVG CEPSDIST (MED) : %6.2f dB\n', avg_cd_med);
%     fprintf(fids(m), 'AVG SRMR           : %6.2f\n'   , avg_srmr_mean);
%     fprintf(fids(m), 'AVG LLR      (MEAN): %6.2f\n'   , avg_llr_mean);
%     fprintf(fids(m), 'AVG LLR      (MED) : %6.2f\n'   , avg_llr_med);
%     fprintf(fids(m), 'AVG FWSEGSNR (MEAN): %6.2f dB\n', avg_snr_mean);
%     fprintf(fids(m), 'AVG FWSEGSNR (MED) : %6.2f dB\n', avg_snr_med);
% 
%     fprintf(fids(m), 'AVG PESQ           : %6.2f\n'   , avg_pesq_mean);
% end

if ~isempty(fid)
    fclose(fid);
end
