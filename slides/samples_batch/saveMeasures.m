%
%
% create spectrograms
%
%


% measures
refClean = audioread('clean.wav');
myMeasures = @(varSig) myScore(varSig, refClean, fs, [], [], 'none');

% output file
fileName = 'measures.txt';
fOut = fopen(fileName,'w+');

% header
fprintf(fOut,'file name \t\t |  PESQ  |   CD   | FWSSNR |   LLR  |  SRMR  | \n');
fprintf(fOut,'          \t\t ----------------------------------------------\n');


% allFiles = {};
allFiles = {
    'noiseless_mic.wav','noiseless_batch.wav','noiseless_batch_nmf.wav', ...
    'noisy_mic.wav','noisy_batch.wav','noisy_batch_nmf.wav' ...
    };

for myFile=allFiles
    
    myFile = myFile{:};
    
    sig = audioread(myFile);
    sig = sig(:,1);
    
    res = myMeasures(sig);
    
    fprintf(fOut,'%20s \t | %6.2f | %6.2f | %6.2f | %6.2f | %6.2f | \n',...
            myFile(1:end-4), res.pesq_mean, res.cd_mean, res.snr_mean, res.llr_mean, res.srmr_mean);
    
end

fprintf(fOut,'          \t\t ----------------------------------------------\n');
fclose(fOut);
