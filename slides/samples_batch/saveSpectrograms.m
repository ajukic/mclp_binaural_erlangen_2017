%
%
% create spectrograms
%
%


dbRange = 80;
myCmap = 'default';
% myCmap = 'jet';

% allFiles = {
%     'clean.wav', ...
%     'noiseless_mic.wav','noiseless_batch.wav','noiseless_batch_nmf.wav', ...
%     'noisy_mic.wav','noisy_batch.wav','noisy_batch_nmf.wav' ...
%     };

allFiles = {'clean.wav'};

for myFile=allFiles
    
    myFile = myFile{:};
    
    [sig, fs] = audioread(myFile);
    sig = sig(:,1);
    
    figure('Name',myFile)
    colormap(myCmap);
    
    sgram(sig,fs,dbRange)
    
    set(gcf,'Color','w')
    
    export_fig([ 'spec_' myCmap '_' myFile(1:end-4) '.eps' ])
    
end