function out = repoRoot()
% Get the root directory of the repository.
%
    out = mfilename('fullpath');
    out = fileparts(out);
end

