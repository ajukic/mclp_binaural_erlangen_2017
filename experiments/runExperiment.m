 function [] = runExperiment(Lg)
% run the experiment and evaluate the measures for the given data type
%
%
%

%% system setup

numMics = 2;
selMics = [1, 2];

fs       = 16e3;
RT60     = [];
timeLate = [];

% STFT
winLen   = 64e-3;
winShift = 16e-3;
winType  = 'hann';
nFFT     = round( winLen * fs );
nShift   = round( winShift * fs );
% use a tight window for analysis and synthesis
win         = {'tight',winType,nFFT};

% common parameters
F         = nFFT/2 + 1;
filtLen   = Lg;
predDelay = 2;
pnorm     = 0;
myeps     = 1e-6;


%% load the data

data = load([ repoRoot '/data/scenario' ]);

% selected microphones
xRev     = data.y(:,selMics);
% warm-up using the same sequence
xRevWarm = data.y(:,selMics);


%% directory for the output


% make a directory for audio output
audioOutDir = [ repoRoot '/audio_out/Lg_' num2str(Lg) ];
mkdir( audioOutDir )

% save the input file
% - normalize the max to 0.8
audiowrite([audioOutDir '/in.wav'], 0.8 * xRev/max(abs(xRev(:))), fs);


%% STFT analysis

% create function for analysis
myAnalysis  = @(sig) dgtreal(sig, win, nShift, nFFT);

% analysis transform
[XRev,Lx] = myAnalysis(xRev);
XRevWarm  = myAnalysis(xRevWarm);

% create function for synthesis
mySynthesis = @(sig) idgtreal(sig, win, nShift, nFFT, Lx);


%% batch processing

% initialize the processor
% ------------------------------------------------------------------------------

% parameters
pBatch.numMics     = numMics;
pBatch.numFreqBins = F;
pBatch.filtLen     = filtLen;
pBatch.predDelay   = predDelay;
pBatch.weightType  = 'iter';
pBatch.pnorm       = pnorm;
pBatch.myeps       = myeps;
pBatch.RT60        = RT60;
pBatch.itMax       = 1;
pBatch.fs          = fs;
pBatch.timeLate    = timeLate;
pBatch.winShift    = winShift;
pBatch.smRev       = 0;
pBatch.smDes       = 0;

% construct the processor
Batch_GWPE = BATCH_GWPE(pBatch);


% parameters for the loop
% ------------------------------------------------------------------------------

% IRLS iterations
itMaxAll = [1, 5, 10];

% evaluatiation
% ------------------------------------------------------------------------------

% batMeasClean = zeros( length(itMaxAll), 1 );
% batMeasEarly = zeros( length(itMaxAll), 1 );

for ii=1:length(itMaxAll)

        % set the parameters
        Batch_GWPE.itMax = itMaxAll(ii);

        % run batch
        batLin = Batch_GWPE.Process( XRev );

        % synthesis
        batLin = mySynthesis( batLin );
        
        % normalize the max to 0.8
        batLin = 0.8 * batLin / max(abs(batLin(:)));

        % save audio
        audiowrite( sprintf('%s/bat_iter_%02d.wav', audioOutDir, Batch_GWPE.itMax), batLin, fs )

end


%% recursive processing

% initialize the processor
% ------------------------------------------------------------------------------

% parameters
pRec.numMics     = numMics;
pRec.numFreqBins = F;
pRec.filtLen     = filtLen;
pRec.predDelay   = predDelay;
pRec.weightType  = 'rev';   % use reverberant power for the weights
pRec.pnorm       = pnorm;
pRec.myeps       = myeps;
pRec.gam         = 0.9;
pRec.fs          = fs;
pRec.RT60        = [];
pRec.timeLate    = [];
pRec.winShift    = winShift;
pRec.constrType  = 'Lebart';
pRec.ADMM_itMax  = 0;
pRec.ADMM_rho    = 1000;
pRec.smType      = 'spec';
pRec.smRev       = 0.65;
pRec.smDes       = 0.65;

% construct the processor
ConRec_GWPE = CON_REC_GWPE(pRec);


% parameters for the loop
% ------------------------------------------------------------------------------

% all forgetting factors
gamAll = [0.94 0.95 0.96 0.97 0.98];

% ADMM iterations
smRevAll = [0.5 0.6 0.7 0.8];

% evaluation
% ------------------------------------------------------------------------------

for ii=1:length(gamAll)
    for jj=1:length(smRevAll)

        % set the parameters
        % ------------------------------------------------------------------
        ConRec_GWPE.gam   = gamAll(ii);
        ConRec_GWPE.smRev = smRevAll(jj);

        % spectral smoothing
        % ------------------------------------------------------------------

        % run recursive with warm start
        recLin = ConRec_GWPE.Process( [XRevWarm, XRev] );

        % throw away the initial part + synthesis
        recLin = mySynthesis( recLin(: , size(XRevWarm,2)+1:end, :) );
        
        % normalize the max to 0.8
        recLin = 0.8 * recLin / max(abs(recLin(:)));

        % save audio
        audiowrite( sprintf('%s/rec_spec_smRev_%d_forgInd_%d_.wav', audioOutDir, jj, ii), recLin, fs )
        
    end
end

