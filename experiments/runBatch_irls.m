 function [] = runBatch_irls(dataType,Lg,itMax)
% run the experiment and evaluate the measures for the given data type
%
%
%

%% system setup

fs       = 16e3;

% STFT
winLen   = 64e-3;
winShift = 16e-3;
winType  = 'hann';
nFFT     = round( winLen * fs );
nShift   = round( winShift * fs );

% common parameters
predDelay = 2;
pnorm     = 0;
myeps     = 1e-6;


%% load the data
% crop 1.5 at the input
%

switch dataType
    case 'clean'
        data = load([ repoRoot '/data/x+n' ]);
        xRev = data.x(:,:);
        % crop the zeros
        xRev = xRev(2*fs:end, :);
    case 'noisy'
        data = load([ repoRoot '/data/scenario' ]);
        xRev = data.y(:,:);
        % crop a bit of noise
        xRev = xRev(1.5*fs:end, :);
end

%% directory for the output


% make a directory for audio output
audioOutDir = [ repoRoot '/audio_out/' dataType '_Lg_' num2str(Lg) ];
mkdir( audioOutDir )

myScale = max(abs(xRev(:)));

function [] = myAudioWrite(varName,varX)
    % % crop 1.5s
    % varX = varX(1.5*fs:end,:);
    % save
    audiowrite( varName, 0.8 * varX / myScale, fs );
end

% save the input file
% - normalize the max to 0.8
myAudioWrite([audioOutDir '/in.wav'], xRev);


%% batch processing

% initialize the processor
% ------------------------------------------------------------------------------

% general settings
opts_WPE.tau    = predDelay;
opts_WPE.Lg     = Lg;
opts_WPE.nFFT   = nFFT;
opts_WPE.nShift = nShift;
opts_WPE.myeps  = myeps;
opts_WPE.relTol = 0;
% IRLS settings
opts_WPE.p     = pnorm;
opts_WPE.itMax = itMax;


% local
% ------------------------------------------------------------------------------
batLoc = alg_nb_irls(xRev,opts_WPE);

myAudioWrite([audioOutDir '/batLoc_it' num2str(itMax) '.wav'], batLoc);

% social
% ------------------------------------------------------------------------------
opts_WPE_soc = opts_WPE;
opts_WPE_soc.socialNeigh  = ones(3,3);
opts_WPE_soc.socialCenter = [2,2];

batSoc = alg_nb_irls_social(xRev,opts_WPE_soc);

myAudioWrite([audioOutDir '/batSoc_it' num2str(itMax) '.wav'], batSoc);

% NMF
% ------------------------------------------------------------------------------

opts_WPE_nmf = opts_WPE;

for orderNMF=[30]
    opts_WPE_nmf.orderNMF = orderNMF;
    batNmf = alg_nb_irls_nmf(xRev,opts_WPE_nmf);

    myAudioWrite([audioOutDir '/batNmf_it' num2str(itMax) '_order_' num2str(orderNMF) '.wav'], batNmf);
end


 end