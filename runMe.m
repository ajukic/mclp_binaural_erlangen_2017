

% local toolboxes
addpath( fullfile(pwd,'algorithms') )
addpath( fullfile(pwd,'experiments') )

addpath( fullfile(pwd,'helper') )

% measures from the reverb challenge
addpath( genpath( fullfile(pwd,'helper/reverb') ) )

% srmr
addpath( fullfile(pwd,'helper/SRMRToolbox-master') )
addpath( genpath( fullfile(pwd,'helper/SRMRToolbox-master/libs') ) )
% compile the mex files
run('./helper/SRMRToolbox-master/gen_mexfiles')
